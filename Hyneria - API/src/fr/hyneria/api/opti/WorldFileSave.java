package fr.hyneria.api.opti;

import fr.hyneria.api.utils.common.ReflectionHandler;
import fr.hyneria.api.utils.common.ReflectionHandler.PackageType;

public class WorldFileSave {
	
	public WorldFileSave() {
		try {
			ReflectionHandler.getClass("RegionFileCache", PackageType.MINECRAFT_SERVER).getMethod("a").invoke(null);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
}
