package fr.hyneria.api.games.gameserver;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.games.HyneriaGame;
import fr.hyneria.api.games.LineStatus;
import fr.hyneria.api.log.LoggerUtils;

public class GameServerLocal extends GameServer {

	static String prefix = "[LOCAL-GS] �6";
	boolean fakeCancelPlayerCanDownloadMap = false;

	@Override
	public void reboot() {
		for (Player player : HyneriaAPI.getInstance().getServer().getOnlinePlayers())
			player.kickPlayer("�6Reboot");
		LoggerUtils.info(prefix + "Reboot !");
		Bukkit.shutdown();
	}

	@Override
	public void readyEvent(HyneriaGame game) {
		this.map = game.getDefaultMap();
		LoggerUtils.info(prefix + "Information envoy�e aux hubs pour enregistrer le serveur en tant que jeu!");
	}

	@Override
	public void setLine3(LineStatus statut) {
		LoggerUtils.info(prefix + "Ligne 3 chang�e : " + statut.toString());
	}

	@Override
	public void setLine4(String string) {
		LoggerUtils.info(prefix + "Ligne 4 chang�e : " + string);
	}

	@Override
	public void setJoinable(boolean b) {
		LoggerUtils.info(prefix + "Serveur mis en mode �eJOIN�6 : " + (b ? "�aoui" : "�cnon"));
	}

	@Override
	public void setPlayable(boolean b) {
		LoggerUtils.info(prefix + "Serveur mis en mode �eJOUABLE�6 : " + (b ? "�aoui" : "�cnon"));
	}

	@Override
	public LineStatus getLine3() {
		LoggerUtils.info(prefix + "R�cup�ration de la ligne 3 : " + this.line3.getStyle());
		return this.line3;
	}

	@Override
	public String getLine4() {
		LoggerUtils.info(prefix + "R�cup�ration de la ligne 4 : " + this.line4);
		return this.line4;
	}

	@Override
	public boolean isJoinable() {
		LoggerUtils.info(prefix + "R�cuparation du mode �eJOIN�6 : " + (this.isJoinable ? "�aoui" : "�cnon"));
		return this.isJoinable;
	}

	@Override
	public boolean isPlayable() {
		LoggerUtils.info(prefix + "R�cuparation du mode �eJOUABLE�6 : " + (this.isPlayable ? "�aoui" : "�cnon"));
		return this.isPlayable;
	}

	@Override
	public String getMap(String name) {
		LoggerUtils.info(prefix + "R�cup�ration de la map : �e" + name);
		return name;
	}

	@Override
	public void waitForPlayerReconnection(Player player, int seconds) {
		String playerName = player.getName();
		playerReconnection.put(playerName, System.currentTimeMillis() + (1000L * seconds));
		LoggerUtils.info(prefix + "Mise de la reconnexion au joueur " + playerName + " pendant " + seconds + " secondes.");
	}

	@Override
	public boolean isWaitingReconnection(Player player) {
		String playerName = player.getName();
		boolean result = playerReconnection.containsKey(playerName) && playerReconnection.get(playerName) >= System.currentTimeMillis();
		LoggerUtils.info(prefix + "Attente de reconnexion pour " + playerName + " : " + (result ? "�aoui" : "�cnon"));
		return result;
	}

	@Override
	public void readyNoGameEvent(String systemName) {
		LoggerUtils.info(prefix + "Information envoy�e aux hubs pour enregistrer le serveur en tant que noGame!");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void callPluginsList(CommandSender sender) {
		// On change la liste des plugins et on affiche SEULEMENT EN LOCAL
		// les plugins qu'on utilise pour StandBlock et pas les autres
		List<Plugin> plugins = new ArrayList<>();
		for (Plugin plugin : HyneriaAPI.getInstance().getServer().getPluginManager().getPlugins())
			if (plugin.getDescription().getDepend().contains("HyneriaAPI") || plugin.getDescription().getName().startsWith("Hyneria"))
				plugins.add(plugin);
		if (plugins.size() == 0) {
			sender.sendMessage("Plugins (0): ");
			return;
		}
		String result = "Plugins (" + plugins.size() + "): �a" + plugins.get(0).getDescription().getRawName();
		for (Plugin plugin : plugins) {
			if (plugin.equals(plugins.get(0))) continue;
			String format = plugin.isEnabled() ? "�a" : "�c";
			result += "�r, " + format;
			for (Character character : plugin.getDescription().getRawName().split(" ")[0].toCharArray())
				if (Character.isLetter(character)) result += character.toString();
		}
		sender.sendMessage(result);
	}
}