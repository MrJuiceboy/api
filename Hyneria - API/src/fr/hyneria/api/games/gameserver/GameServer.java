package fr.hyneria.api.games.gameserver;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.caches.CacheModule;
import fr.hyneria.api.games.HyneriaGame;
import fr.hyneria.api.games.LineStatus;

public abstract class GameServer extends CacheModule {
	
	// STATICS FIELDS
	private static GameServer     		instance;
	public static String	bilip			= "§e§k!§b§k!§a§k!§c§k!§d§k!";
	public static String	line			= "-----------------------------------------------------";
	
	
	// FIELDS
	protected	   Map<String, Long>	playerReconnection 		= new HashMap<String, Long>();
	protected 	   String         		saveTimeServer 			= "";	
	protected  	   LineStatus	  		line3 					= LineStatus.WAITING;
	protected      String		  		line4 					= "";
	protected      boolean		  		isJoinable 				= true;
	protected      boolean		  		isPlayable 				= true;
	protected      String	      		map;
	
	// FIELDS DES STATUTS
	public String			statusWaiting	= "§a§l► Entrer ◄";
	public String			statusRunning	= "§e§l☼ Ingame ☼";
	public String			statusSpectator	= "§7§lSpectateur";
	public String			statusReboot	= "§c§l• Reboot •";
	public String			statusStop		= "§4§lHors-ligne";

	/**
	 * Set the name of the system (example: tower, hub..)
	 * @param serverName
	 */
	public void setSystemTimeSaver(String serverName) {
		this.saveTimeServer = serverName;
	}
	
	// Getter
	/**
	 * Get the system name, just for database logging.
	 * @return a string
	 */
	public String getSystemTimeSaver() {
		return this.saveTimeServer;
	}
	
	/**
	 * If this server going to have or just having a saving time on database system.
	 * @return
	 */
	public boolean haveTimeSaver() {
		return !this.saveTimeServer.equals("");
	}
	
	/**
	 * Reboot du serveur en kickant les joueurs
	 * (à utiliser en étant sûr que les joueurs ont été retp au hub)
	 */
	public abstract void reboot();
	
	/**
	 * Envoi les données au hub et le relier au serveur à la demande
	 *
	 */
	public abstract void readyEvent(HyneriaGame game);
	
	/**
	 * Envoi les données au hub et le relier au serveur à la demande
	 * @param systemName > nom du système
	 * @deprecated > A UTILISER SEULEMENT POUR LES SERVEURS QUI NE SONT PAS DES JEUX
	 */
	public abstract void readyNoGameEvent(String systemName);
	
	/**
	 * Activer ou désactiver le déchargement automatique des chunks (pour les garder en mémoire ou non)
	 * @param value
	 */
	public void enableAutoUnloadChunk(boolean value) {
		for (World world : HyneriaAPI.getInstance().getServer().getWorlds())
			world.setAutoSave(value);
	}
	
	/**
	 * Rajoute un joueur dans le mode d'auto-reconnexion, s'il se reconnecte avant l'expiration il
	 * retournera automatiquement sur la partie où il était, purge du système automatiquement ou
	 * après le temps imparti
	 * (mode auto-reconnexion sauvegardé en mémoire cache dans le network)
	 * @param playerName > nom du joueur
	 * @param seconds
	 */
	public abstract void waitForPlayerReconnection(Player player, int seconds);
	
	/**
	 * Vérifier si le joueur peut se reconnecter suite au mode auto-reconnexion
	 * @param playerName > nom du joueur
	 * @return si le joueur peut se reconnecter alors que la partie est en cours
	 */
	public abstract boolean isWaitingReconnection(Player player);
	
	/**
	 * Modifier la troisième ligne des informations du serveur
	 * @param status
	 */
	public abstract void setLine3(LineStatus status);
	
	/**
	 * Modifier la quatrième ligne des informations du serveur
	 * @param status
	 */
	public abstract void setLine4(String line);
	
	/**
	 * Dire au noyeau que votre serveur peut être rejoint depuis le hub
	 * @param b
	 */
	public abstract void setJoinable(boolean b);
	
	/**
	 * Dire si la partie est jouable ou non
	 * @param b
	 */
	public abstract void setPlayable(boolean b);

	/**
	 * Récupérer le nom de la map où le serveur tourne
	 * @param name
	 * @return
	 */
	public abstract String getMap(String name);
	
	/**
	 * Demander la liste des plugins au système de gestion
	 * du serveur à la demande
	 * @param sender
	 */
	public abstract void callPluginsList(CommandSender sender);
	
	// GETTERS
	/**
	 * Retourne le status de la partie
	 * @return
	 */
	public LineStatus getLine3() {
		return this.line3;
	}
	
	/**
	 * Retourne la quatrième ligne des informations du serveur
	 * @return
	 */
	public String getLine4() {
		return this.line4;
	}
	
	/**
	 * Retourne si la partie est rejoignable ou non
	 * @return
	 */
	public boolean isJoinable() {
		return this.isJoinable;
	}
	
	/**
	 * Retourne si la partie est jouable ou non
	 * @return
	 */
	public boolean isPlayable() {
		return this.isPlayable;
	}
	
	/**
	 * Récupérer la map
	 * @return
	 */
	public String getMap() {
		return this.map;
	}
	
	/**
	 * Récupérer l'instance de GameServer
	 * @return
	 */
	public static GameServer getInstance() {
		if (instance == null) instance = new GameServerLocal();
		return instance;
	}
	
	/**
	 * Changer de GameSERVER
	 * @deprecated
	 * @param gameServer
	 */
	protected static void setGameServer(GameServer gameServer) {
		instance = gameServer;
	}
	
	/**
	 * Dit si le module GameServer est connecté à l'infrastructure ou si c'est en local
	 * @return
	 */
	@Override
	public boolean isConnected() {
		return !(instance instanceof GameServerLocal);
	}
	
}
