package fr.hyneria.api.games;

import java.util.Arrays;
import java.util.List;

import fr.hyneria.api.games.exception.UnknownHyneriaGame;

public enum HyneriaGame {
	
	/**
	 * Veuillez ajouter vos jeux
	 */
	HUNGERGAMES			("HungeerGames", "default", "MrJuiceboy", Arrays.asList("default"));
	
	// FIELDS
	private String 			gameName;
	private String 			defaultMap;
	private String 			developper;
	private List<String> 	maps;
	
	/**
	 * Constructor of the StandGame
	 * @param gameName > The name of the game
	 * @param defaultMap > The default map
	 * @param developper > The developper (minecraft nickname)
	 * @param maps > Getting the maps list
	 */
	private HyneriaGame (String gameName, String defaultMap, String developper, List<String> maps) {
		this.gameName = gameName;
		this.defaultMap = defaultMap;
		this.developper = developper;
		this.maps = maps;
	}
	
	/**
	 * Getting the name
	 * @return
	 */
	public String getName() {
		return this.gameName;
	}
	
	/**
	 * Getting the default map of the game
	 * @return
	 */
	public String getDefaultMap() {
		return this.defaultMap;
	}
	
	/**
	 * Getting the developper nickname (minecraft name)
	 * @return
	 */
	public String getDevelopper() {
		return this.developper;
	}
	
	/**
	 * Getting the maps
	 * @return
	 */
	public List<String> getMaps() {
		return this.maps;
	}
	
	/**
	 * Get standgame
	 * @param game
	 * @return
	 */
	public static HyneriaGame get(String game) {
		for (HyneriaGame standGame : values())
			if (standGame.getName().equalsIgnoreCase(game)) return standGame;
		throw new UnknownHyneriaGame(game);
	}
	
}
