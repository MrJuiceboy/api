package fr.hyneria.api.games;

/**
 * LineStatus (pour avoir quelque chose de concret et symétrique au niveau des messages des 
 * disponibilités de serveurs au hub)
 */
public enum LineStatus {
	// ENUM
	WAITING("§a§l► Entrer ◄"), 
	RUNNING("§e§l☼ Ingame ☼"), 
	SPECTATOR("§7§lSpectateur"), 
	REBOOT("§c§l• Reboot •"), 
	STOP("§4§lHors-ligne"), 
	CUSTOM("");
	
	// FIELDS
	String	nom	= "";
	String	style;
	
	/**
	 * Constructeur privé
	 * @param style
	 */
	LineStatus(String style) {
		this.nom = this.nom();
		this.style = style;
	}
	
	/**
	 * Récupérer le style du status
	 * @return
	 */
	public String getStyle() {
		return this.style;
	}
	
	/**
	 * Mettre un nom custom
	 * @deprecated déconseillé
	 * @param name
	 * @return
	 */
	public LineStatus setCustomName(String name) {
		this.nom = name;
		return this;
	}

	/**
	 * Mettre un style custom
	 * @deprecated déconseillé
	 * @param name
	 * @return
	 */
	public LineStatus setStyle(String style) {
		this.style = style;
		return this;
	}
	
	/**
	 * Récupérer le nom
	 * @return
	 */
	public String nom() {
		if ("".equals(nom))
			return this.name().toLowerCase();
		return nom;
	}
	
	/**
	 * Récupérer un statut spécifique
	 * @param nom
	 * @return
	 */
	public static LineStatus getStatut(String nom) {
		for (LineStatus s : LineStatus.values())
			if (s.nom().equalsIgnoreCase(nom))
				return s;
		return CUSTOM.setCustomName(nom).setStyle(nom);
	}
	
}