package fr.hyneria.api.games.manager;

import fr.hyneria.api.games.LineStatus;
import fr.hyneria.api.games.gameserver.GameServer;

/**
 * Syst�me simpliste pour ceux qui ne
 * comprennent pas le fonctionnement exact de 
 * isJoinable/isPlayable pour envoyer au hub le statut
 */
public class GStatusManager {
	
	// GameServer
	private GameServer		gameServer;
	
	/**
	 * Constructeur prot�g� du GStatusManager
	 * @param gameServer
	 */
	GStatusManager(GameServer gameServer) {
		this.gameServer = gameServer;
	}

	/**
	 * Envoyer au hub le signal que votre jeu est en attente de joueurs (m�me si c'est plein)
	 */
	public void setWaiting() {
		gameServer.setLine3(LineStatus.WAITING);
		gameServer.setJoinable(true);
		gameServer.setPlayable(false);
	}

	/**
	 * Envoyer au hub le signal que votre jeu est en cours
	 * @spectator > mettre true pour autoriser le support spec, sinon mettre false
	 */
	public void setRunning(boolean spectator) {
		// Spectateur
		if (spectator) {
			gameServer.setLine3(LineStatus.SPECTATOR);
			gameServer.setJoinable(false);
			gameServer.setPlayable(true);
			return;
		}
		gameServer.setLine3(LineStatus.RUNNING);
		gameServer.setJoinable(false);
		gameServer.setPlayable(true);
	}

	/**
	 * Envoyer au hub le signal que votre jeu est termin�
	 */
	public void setFinish() {
		gameServer.setLine3(LineStatus.STOP);
		gameServer.setJoinable(false);
		gameServer.setPlayable(false);
	}
	
}
