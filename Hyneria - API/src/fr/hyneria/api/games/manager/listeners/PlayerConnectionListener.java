package fr.hyneria.api.games.manager.listeners;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.listeners.HyneriaListener;
import fr.hyneria.api.listeners.customs.callers.events.players.join.PlayerJoinedEvent;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerConnectionListener extends HyneriaListener {

    /**
     * Renvoi du packet de join quand le joueur est invisible pour un autre
     * (arrive surtout en party)
     * @param player
     */
    public void reSendPlayerJoined(Player player) {
        // Test de renvois du packet
        final String playerName = player.getName();
        Bukkit.getScheduler().runTaskLater(HyneriaAPI.getInstance(), new Runnable() {
            @Override
            public void run() {
                Player p = Bukkit.getPlayer(playerName);
                if (p == null) return;
                EntityPlayer[] e = new EntityPlayer[]{((CraftPlayer) p).getHandle()};
                PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, e);
                for (Player online : Bukkit.getOnlinePlayers()) {
                    if (online.getUniqueId().equals(p.getUniqueId())) continue;
                    EntityPlayer ep = ((CraftPlayer)online).getHandle();
                    if (ep.playerConnection != null && !ep.playerConnection.isDisconnected()) ep.playerConnection.sendPacket(packet);
                }
            }
            // D�calage sur 5 sec
        }, 20 * 5);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        reSendPlayerJoined(player);
    }

    @EventHandler (ignoreCancelled = true)
    public void onPlayerJoined(PlayerJoinedEvent event) {
        Player player = event.getPlayer();
        HyneriaAPI.getInstance().getPacketManager().loadPlayer(event.getHyneriaPlayer());
    }

}
