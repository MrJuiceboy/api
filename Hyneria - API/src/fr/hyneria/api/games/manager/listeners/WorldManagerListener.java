package fr.hyneria.api.games.manager.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager de mondes simpliste pour cancel des events
 */
public class WorldManagerListener extends HyneriaListener {

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onworldChunkLoad(ChunkLoadEvent event) {
		if (!GameManager.getInstance().canWorldChunkLoad) event.getChunk().unload();
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWorldChunkUnload(ChunkUnloadEvent event) {
		if (!GameManager.getInstance().canWorldChunkUnload) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWorldPortalCreate(PortalCreateEvent event) {
		if (!GameManager.getInstance().canWorldPortalCreate) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWorldStructureGrow(StructureGrowEvent event) {
		if (!GameManager.getInstance().canWorldStructureGrow) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWorldSave(WorldSaveEvent event) {
		event.getWorld().setAutoSave(GameManager.getInstance().canWorldSave);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWorldUnload(WorldUnloadEvent event) {
		if (!GameManager.getInstance().canWorldUnload) event.setCancelled(true);
	}
	
}
