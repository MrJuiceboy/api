package fr.hyneria.api.games.manager.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.vehicle.VehicleCreateEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager de véhicules simpliste pour cancel des events
 */
public class VehicleManagerListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleCreate(VehicleCreateEvent event) {
		if (!GameManager.getInstance().canVehicleCreate) event.getVehicle().remove();
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleDamage(VehicleDamageEvent event) {
		if (!GameManager.getInstance().canVehicleDamage) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleDestroy(VehicleDestroyEvent event) {
		if (!GameManager.getInstance().canVehicleDestroy) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleEnter(VehicleEnterEvent event) {
		if (!GameManager.getInstance().canVehicleEnter) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleEntityCollision(VehicleEntityCollisionEvent event) {
		if (!GameManager.getInstance().canVehicleEntityCollision) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onVehicleExit(VehicleExitEvent event) {
		if (!GameManager.getInstance().canVehicleExit) event.setCancelled(true);
	}
	
}
