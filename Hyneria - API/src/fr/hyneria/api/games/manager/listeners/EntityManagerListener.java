package fr.hyneria.api.games.manager.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustByBlockEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityCreatePortalEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.entity.HorseJumpEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PigZapEvent;
import org.bukkit.event.entity.PlayerLeashEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.entity.SheepDyeWoolEvent;
import org.bukkit.event.entity.SheepRegrowWoolEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager d'entit�es simpliste pour cancel des events
 */
public class EntityManagerListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onCreatureSpawn(CreatureSpawnEvent event) {
		if (!GameManager.getInstance().canCreatureSpawn) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityBreakDoor(EntityBreakDoorEvent event) {
		if (!GameManager.getInstance().canEntityBreakDoor) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityChangeBlock(EntityChangeBlockEvent event) {
		if (!GameManager.getInstance().canEntityChangeBlock) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityCombustByBlock(EntityCombustByBlockEvent event) {
		if (!GameManager.getInstance().canEntityCombustByBlock) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityCombustByEntity(EntityCombustByEntityEvent event) {
		if (!GameManager.getInstance().canEntityCombustByEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityCreatePortal(EntityCreatePortalEvent event) {
		if (!GameManager.getInstance().canEntityCreatePortal) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityDamageByBlock(EntityDamageByBlockEvent event) {
		if (!GameManager.getInstance().canEntityDamageByBlock) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!GameManager.getInstance().canEntityDamageByEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityDamaged(EntityDamageEvent event) {
		if (!GameManager.getInstance().canEntityDamaged && (event.getEntity().getType() == EntityType.PLAYER)) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityExplode(EntityExplodeEvent event) {
		if (!GameManager.getInstance().canEntityExplode) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityInteract(EntityInteractEvent event) {
		if (!GameManager.getInstance().canEntityInteract) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityPortalExit(EntityPortalExitEvent event) {
		if (!GameManager.getInstance().canEntityPortalExit) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityRegainHealth(EntityRegainHealthEvent event) {
		if (!GameManager.getInstance().canEntityRegainHealth) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityShootBow(EntityShootBowEvent event) {
		if (!GameManager.getInstance().canEntityShootBow) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntitySpawn(EntitySpawnEvent event) {
		if (!GameManager.getInstance().canEntitySpawn) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityTarget(EntityTargetEvent event) {
		if (!GameManager.getInstance().canEntityTarget) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
		if (!GameManager.getInstance().canEntityTargetLivingEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onEntityTeleport(EntityTeleportEvent event) {
		if (!GameManager.getInstance().canEntityTeleport) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onExpBottle(ExpBottleEvent event) {
		if (!GameManager.getInstance().canExpBottle) event.setExperience(0);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onExplosionPrime(ExplosionPrimeEvent event) {
		if (!GameManager.getInstance().canExplosionPrime) event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onHorseJump(HorseJumpEvent event) {
		if (!GameManager.getInstance().canHorseJump) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onItemDespawn(ItemDespawnEvent event) {
		if (!GameManager.getInstance().canItemDespawn) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onItemSpawn(ItemSpawnEvent event) {
		if (!GameManager.getInstance().canItemSpawn) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPigZap(PigZapEvent event) {
		if (!GameManager.getInstance().canPigZap) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerLeashEntity(PlayerLeashEntityEvent event) {
		if (!GameManager.getInstance().canPlayerLeashEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPotionSplash(PotionSplashEvent event) {
		if (!GameManager.getInstance().canPotionSplash) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (!GameManager.getInstance().canProjectileLaunch) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSheepDyeWool(SheepDyeWoolEvent event) {
		if (!GameManager.getInstance().canSheepDyeWool) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSheepRegrowWool(SheepRegrowWoolEvent event) {
		if (!GameManager.getInstance().canSheepRegrowWool) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSlimeSplit(SlimeSplitEvent event) {
		if (!GameManager.getInstance().canSlimeSplit) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onSpawnerSpawn(SpawnerSpawnEvent event) {
		if (!GameManager.getInstance().canSpawnerSpawn) event.setCancelled(true);
	}
	
}
