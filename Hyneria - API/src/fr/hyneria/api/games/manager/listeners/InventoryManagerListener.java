package fr.hyneria.api.games.manager.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.inventory.FurnaceExtractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.ItemStack;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager d'inventaire simpliste pour cancel des events
 */
public class InventoryManagerListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onCraftItem(CraftItemEvent event) {
		if (!GameManager.getInstance().canCraftItem) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onFurnaceBurn(FurnaceBurnEvent event) {
		if (!GameManager.getInstance().canFurnaceBurn) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onFurnaceExtract(FurnaceExtractEvent event) {
		if (!GameManager.getInstance().canFurnaceExtract) event.setExpToDrop(0);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryClick(InventoryClickEvent event) {
		if (!GameManager.getInstance().canInventoryClick) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryCreative(InventoryCreativeEvent event) {
		if (!GameManager.getInstance().canInventoryCreative) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryDrag(InventoryDragEvent event) {
		if (!GameManager.getInstance().canInventoryDrag) event.setCancelled(true);
	}	

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryInteract(InventoryInteractEvent event) {
		if (!GameManager.getInstance().canInventoryInteract) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryMoveItem(InventoryMoveItemEvent event) {
		if (!GameManager.getInstance().canInventoryMoveItem) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryOpen(InventoryOpenEvent event) {
		if (!GameManager.getInstance().canInventoryOpen) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onInventoryPickupItem(InventoryPickupItemEvent event) {
		if (!GameManager.getInstance().canInventoryPickupItem) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPrepareItemCraft(PrepareItemCraftEvent event) {
		if (!GameManager.getInstance().canPrepareItemCraft) {
			event.getInventory().setResult(new ItemStack(Material.AIR));
			for (HumanEntity humanEntity : event.getViewers()) {
				if (humanEntity.getType() == EntityType.PLAYER)
					((Player)humanEntity).sendMessage(ChatColor.RED + "Le craft est désactivé.");
			}
		}
	}

}
