package fr.hyneria.api.games.manager.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager de temps simpliste pour cancel des events
 */
public class WeatherManagerListener extends HyneriaListener {

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onLightningStrike(LightningStrikeEvent event) {
		if (!GameManager.getInstance().canLightningStrike) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onThunderChange(ThunderChangeEvent event) {
		if (!GameManager.getInstance().canThunderChange) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onWeatherChange(WeatherChangeEvent event) {
		if (!GameManager.getInstance().canWeatherChange) event.setCancelled(true);
	}
	
}
