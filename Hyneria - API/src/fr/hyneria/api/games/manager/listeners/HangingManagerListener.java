package fr.hyneria.api.games.manager.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager de hanging simpliste pour cancel des events
 */
public class HangingManagerListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
		if (!GameManager.getInstance().canHangingBreakByEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onHangingBreak(HangingBreakEvent event) {
		if (!GameManager.getInstance().canHangingBreak) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onHangingPlace(HangingPlaceEvent event) {
		if (!GameManager.getInstance().canHangingPlace) event.setCancelled(true);
	}
	
}
