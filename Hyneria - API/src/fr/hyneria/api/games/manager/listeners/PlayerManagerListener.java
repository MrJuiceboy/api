package fr.hyneria.api.games.manager.listeners;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.event.player.PlayerVelocityEvent;

import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * Manager de joueurs simpliste pour cancel des events
 */
@SuppressWarnings("deprecation")
public class PlayerManagerListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
		if (!GameManager.getInstance().canPlayerChat) event.setCancelled(true);
	}

	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerAnimation(PlayerAnimationEvent event) {
		if (!GameManager.getInstance().canPlayerAnimation) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
		if (!GameManager.getInstance().canPlayerArmorStandManipulate) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerBedEnter(PlayerBedEnterEvent event) {
		if (!GameManager.getInstance().canPlayerBedEnter) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
		if (!GameManager.getInstance().canPlayerBucketEmpty) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerBucketFill(PlayerBucketFillEvent event) {
		if (!GameManager.getInstance().canPlayerBucketFill) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		if (!GameManager.getInstance().canPlayerCommandPreprocess) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (!GameManager.getInstance().canPlayerDropItem) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerEditBook(PlayerEditBookEvent event) {
		if (!GameManager.getInstance().canPlayerEditBook) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerEggThrow(PlayerEggThrowEvent event) {
		if (!GameManager.getInstance().canPlayerEggThrow) event.setNumHatches((byte) 0);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerExpChange(PlayerExpChangeEvent event) {
		if (!GameManager.getInstance().canPlayerExpChange) event.setAmount(0);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerFish(PlayerFishEvent event) {
		if (!GameManager.getInstance().canPlayerFish) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
		if (!GameManager.getInstance().canPlayerGameModeChange) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
		if (!GameManager.getInstance().canPlayerInteractAtEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if (!GameManager.getInstance().canPlayerInteractEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
		if (!GameManager.getInstance().canPlayerItemConsume) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerItemDamage(PlayerItemDamageEvent event) {
		if (!GameManager.getInstance().canPlayerItemDamage) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerItemHeld(PlayerItemHeldEvent event) {
		if (!GameManager.getInstance().canPlayerItemHeld) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerDamage(EntityDamageByEntityEvent event) {
		if (!GameManager.getInstance().canPlayerDamage && event.getDamager().getType() == EntityType.PLAYER) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerDamagedByEntity(EntityDamageByEntityEvent event) {
		if (!GameManager.getInstance().canPlayerDamaged && event.getEntity().getType() == EntityType.PLAYER) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerDamaged(EntityDamageEvent event) {
		if (!GameManager.getInstance().canPlayerDamaged && event.getEntity().getType() == EntityType.PLAYER) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerMove(PlayerMoveEvent event) {
		if (!GameManager.getInstance().canPlayerMove) {
			Location from = event.getFrom();
			Location to = event.getTo();
			if (from.getX() != to.getX() || from.getY() != to.getY() || from.getZ() != to.getZ())
				event.setTo(event.getFrom());
		}
		if (!GameManager.getInstance().canPlayerMoveHead) {
			Location from = event.getFrom();
			Location to = event.getTo();
			if (from.getYaw() != to.getYaw() || from.getPitch() != to.getPitch())
				event.setTo(event.getFrom());
		}
		if (!GameManager.getInstance().canPlayerMoveToAnotherBlock) {
			Location from = event.getFrom();
			Location to = event.getTo();
			if ((int)from.getX() != (int)to.getX() || (int)from.getY() != (int)to.getY() || (int)from.getZ() != (int)to.getZ())
				event.setTo(event.getFrom());
		}
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		if (!GameManager.getInstance().canPlayerPickupItem) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerEnterPortal(PlayerPortalEvent event) {
		if (!GameManager.getInstance().canPlayerEnterPortal) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerShearEntity(PlayerShearEntityEvent event) {
		if (!GameManager.getInstance().canPlayerShearEntity) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		if (!GameManager.getInstance().canPlayerTeleport) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
		if (!GameManager.getInstance().canPlayerToggleFlight) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerToggleSprint(PlayerToggleSprintEvent event) {
		if (!GameManager.getInstance().canPlayerToggleSprint) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
		if (!GameManager.getInstance().canPlayerToggleSneak) event.setCancelled(true);
	}
	
	@EventHandler (priority = EventPriority.LOWEST, ignoreCancelled = true)
	public void onPlayerVelocity(PlayerVelocityEvent event) {
		if (!GameManager.getInstance().canPlayerVelocity) event.setCancelled(true);
	}
	
}
