package fr.hyneria.api.games.manager.server;

import fr.hyneria.api.database.caches.CacheModule;


public abstract class SManager extends CacheModule {

	private static SManager instance		= new SManagerFake();
	
	public abstract void executeLagChecker();
	
	@Override
	public boolean isConnected() {
		return !(instance instanceof SManagerFake);
	}
	
	public static SManager getInstance() {
		return instance;
	}
	
	public static void setInstance(SManager manager) {
		instance = manager;
	}
	
}
