package fr.hyneria.api.games.manager;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.commands.AbstractCommand;
import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.games.manager.listeners.BlocksManagerListener;
import fr.hyneria.api.games.manager.listeners.EnchantManagerListener;
import fr.hyneria.api.games.manager.listeners.EntityManagerListener;
import fr.hyneria.api.games.manager.listeners.HangingManagerListener;
import fr.hyneria.api.games.manager.listeners.InventoryManagerListener;
import fr.hyneria.api.games.manager.listeners.PlayerManagerListener;
import fr.hyneria.api.games.manager.listeners.VehicleManagerListener;
import fr.hyneria.api.games.manager.listeners.WeatherManagerListener;
import fr.hyneria.api.games.manager.listeners.WorldManagerListener;
import fr.hyneria.api.listeners.HyneriaListener;

/**
 * GameManager is a class for facility creating games
 */
public class GameManager {
	
	/** STATICS FIELDS **/
	private static GameManager instance;
	
	/** public FIELDS **/
	// Blocks
	public boolean canBlockPlace = true;
	public boolean canBlockDamage = true;
	public boolean canBlockBreak = true;
	public boolean canBlockBurn = true;
	public boolean canBlockFromTo = true;
	public boolean canBlockGrow = true;
	public boolean canBlockIgnite = true;
	public boolean canBlockMultiPlace = true;
	public boolean canBlockPhysics = true;
	public boolean canBlockPistonExtend = true;
	public boolean canBlockPistonRetract = true;
	public boolean canBlockRedstoneFired = true;
	public boolean canBlockSpread = true;
	public boolean canLeavesDecay = true;
	public boolean canSignChange = true;
	public boolean canNotePlay = true;
	// Enchants
	public boolean canEnchantItem = true;
	public boolean canPrepareItemEnchant = true;
	// Entities
	public boolean canCreatureSpawn = true;
	public boolean canCreeperPower = true;
	public boolean canEntityBreakDoor = true;
	public boolean canEntityChangeBlock = true;
	public boolean canEntityCombustByBlock = true;
	public boolean canEntityCombustByEntity = true;
	public boolean canEntityCreatePortal = true;
	public boolean canEntityDamageByBlock = true;
	public boolean canEntityDamageByEntity = true;
	public boolean canEntityDamaged = true;
	public boolean canEntityExplode = true;
	public boolean canEntityInteract = true;
	public boolean canEntityPortalExit = true;
	public boolean canEntityRegainHealth = true;
	public boolean canEntityShootBow = true;
	public boolean canEntitySpawn = true;
	public boolean canEntityTarget = true;
	public boolean canEntityTargetLivingEntity = true;
	public boolean canEntityTeleport = true;
	public boolean canExpBottle = true;
	public boolean canExplosionPrime = true;
	public boolean canHorseJump = true;
	public boolean canItemDespawn = true;
	public boolean canItemSpawn = true;
	public boolean canPigZap = true;
	public boolean canPlayerLeashEntity = true;
	public boolean canPotionSplash = true;
	public boolean canProjectileLaunch = true;
	public boolean canSheepDyeWool = true;
	public boolean canSheepRegrowWool = true;
	public boolean canSlimeSplit = true;
	public boolean canSpawnerSpawn = true;
	// Hanging
	public boolean canHangingBreakByEntity = true;
	public boolean canHangingBreak = true;
	public boolean canHangingPlace = true;
	// Inventory
	public boolean canCraftItem = true;
	public boolean canFurnaceBurn = true;
	public boolean canFurnaceExtract = true;
	public boolean canFurnaceSmelt = true;
	public boolean canInventoryClick = true;
	public boolean canInventoryCreative = true;
	public boolean canInventoryDrag = true;
	public boolean canInventoryInteract = true;
	public boolean canInventoryMoveItem = true;
	public boolean canInventoryOpen = true;
	public boolean canInventoryPickupItem = true;
	public boolean canPrepareItemCraft = true;	
	// Player
	public boolean canPlayerChat = true;
	public boolean canPlayerAwardAchievement = true;
	public boolean canPlayerAnimation = true;
	public boolean canPlayerArmorStandManipulate = true;
	public boolean canPlayerBedEnter = true;
	public boolean canPlayerBucketEmpty = true;
	public boolean canPlayerBucketFill = true;
	public boolean canPlayerCommandPreprocess = true;
	public boolean canPlayerDropItem = true;
	public boolean canPlayerEditBook = true;
	public boolean canPlayerEggThrow = true;
	public boolean canPlayerExpChange = true;
	public boolean canPlayerFish = true;
	public boolean canPlayerGameModeChange = true;
	public boolean canPlayerInteractAtEntity = true;
	public boolean canPlayerInteractEntity = true;
	public boolean canPlayerItemConsume = true;
	public boolean canPlayerItemDamage = true;
	public boolean canPlayerItemHeld = true;
	public boolean canPlayerDamage = true;
	public boolean canPlayerDamaged = true;
	public boolean canPlayerMove = true;
	public boolean canPlayerMoveToAnotherBlock = true;
	public boolean canPlayerMoveHead = true;
	public boolean canPlayerPickupItem = true;
	public boolean canPlayerEnterPortal = true;
	public boolean canPlayerShearEntity = true;
	public boolean canPlayerTeleport = true;
	public boolean canPlayerToggleFlight = true;
	public boolean canPlayerToggleSneak = true;
	public boolean canPlayerToggleSprint = true;
	public boolean canPlayerVelocity = true;
	// Vehicles
	public boolean canVehicleCreate = true;
	public boolean canVehicleDamage = true;
	public boolean canVehicleDestroy = true;
	public boolean canVehicleEnter = true;
	public boolean canVehicleEntityCollision = true;
	public boolean canVehicleExit = true;
	// Weather
	public boolean canLightningStrike = true;
	public boolean canThunderChange = true;
	public boolean canWeatherChange = true;
	// World
	public boolean canWorldChunkLoad = true;
	public boolean canWorldChunkUnload = true;
	public boolean canWorldPortalCreate = true;
	public boolean canWorldStructureGrow = true;
	public boolean canWorldSave = true;
	public boolean canWorldUnload = true;
	
	// Managers sp�ciaux
	private GStatusManager gameServerStatusManager;
	
	/** CONSTRUCTOR **/
	public GameManager() {
		HyneriaAPI plugin = HyneriaAPI.getInstance();
		registerListener(new BlocksManagerListener(), plugin);
		registerListener(new EnchantManagerListener(), plugin);
		registerListener(new EntityManagerListener(), plugin);
		registerListener(new HangingManagerListener(), plugin);
		registerListener(new InventoryManagerListener(), plugin);
		registerListener(new PlayerManagerListener(), plugin);
		registerListener(new VehicleManagerListener(), plugin);
		registerListener(new WeatherManagerListener(), plugin);
		registerListener(new WorldManagerListener(), plugin);
		// Enregistrement des managers sp�ciaux
		this.gameServerStatusManager = new GStatusManager(GameServer.getInstance());
	}
	
	/**
	 * R�cup�rer l'instance du syst�me de gestion
	 * du status du jeu aux hubs (GameServer)
	 * pour modifier le status !
	 * @return
	 */
	public GStatusManager getGameServerStatusManager() {
		return this.gameServerStatusManager;
	}

	/**
	 * Registering a command
	 * @param abstractCommand
	 */
	public void registerCommand(AbstractCommand abstractCommand) {
		abstractCommand.register();
	}
	
	/**
	 * Registering a listener
	 * @param listener > the listener 
	 * @param pluginBy > the owner plugin
	 */
	public void registerListener(HyneriaListener listener, Plugin pluginBy) {
		PluginManager pluginManager = HyneriaAPI.getInstance().pm;
		pluginManager.registerEvents(listener, pluginBy);
	}
	
	/**
	 * Registering multiple listeners
	 * @param pluginBy > the owner plugin
	 * @param listener > listeners list to be registered
	 */
	public void registerListener(Plugin pluginBy, HyneriaListener... listener) {
		PluginManager pluginManager = HyneriaAPI.getInstance().pm;
		for (HyneriaListener l : listener)
			pluginManager.registerEvents(l, pluginBy);
	}
	
	/** STATIC METHODS **/
	
	/**
	 * Getting the GameManager instance
	 * (singleton)
	 * @return a GameManager object
	 */
	public static GameManager getInstance() {
		if (instance == null) instance = new GameManager();
		return instance;
	}
	
	/**
	 * New instance of gamemanager (singleton)
	 */
	public static void newInstance() {
		getInstance();
	}
	
}
