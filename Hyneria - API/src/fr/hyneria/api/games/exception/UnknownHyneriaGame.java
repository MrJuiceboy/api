package fr.hyneria.api.games.exception;

@SuppressWarnings("serial")
public class UnknownHyneriaGame extends RuntimeException {
	
	public UnknownHyneriaGame(String hyneriaGames) {
		super("'" + hyneriaGames + "' game has been not found !");
	}
	
}
