package fr.hyneria.api.plib;

public enum MessagePacket {

    // Chat packet
    CHAT ((byte) 1),
    // ActionBar packet
    ACTION_BAR ((byte) 2);

    // Field of the packetData
    byte packetData;

    /**
     * Constructor of the message packet enum.
     * @param packetData > the data of the packet
     */
    MessagePacket(byte packetData) {
        // Set the packet data on the field
        this.packetData = packetData;
    }

    /**
     * Get the packetdata in byte
     * @return
     */
    public byte getPacketData() {
        return this.packetData;
    }
}