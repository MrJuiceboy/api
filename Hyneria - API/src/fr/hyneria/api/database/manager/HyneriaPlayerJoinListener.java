package fr.hyneria.api.database.manager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.redis.RedisAccess;
import fr.hyneria.api.listeners.HyneriaListener;
import fr.hyneria.api.players.PlayerFields;

public class HyneriaPlayerJoinListener extends HyneriaListener {
	
	@EventHandler
	public void join(PlayerJoinEvent event){
		Player p = event.getPlayer();
		HyneriaAPI.sql.createAccount(p);
		RedisAccess redisAccess = RedisAccess.instance;
		RedissonClient redissonClient = redisAccess.getRedissonclient();
		RBucket<PlayerFields> accountBucket = redissonClient.getBucket("HyneriaPlayer:" + event.getPlayer().getUniqueId().toString());
		PlayerFields hp = accountBucket.get();
		accountBucket.set(hp);
	}
}
