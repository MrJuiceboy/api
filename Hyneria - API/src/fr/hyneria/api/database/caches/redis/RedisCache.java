package fr.hyneria.api.database.caches.redis;

import fr.hyneria.api.database.caches.CacheModule;
import fr.hyneria.api.database.caches.redis.infos.RedisClassInfo;
import fr.hyneria.api.database.caches.redis.infos.RedisInfo;
import fr.hyneria.api.database.caches.redis.infos.RedisTypeInfo;

public abstract class RedisCache extends CacheModule {

    // Singleton
    public static RedisCache 		instance = new RedisCacheLocal();

    // SETTERS

    /**
     * Changer ou attribuer une valeur d'une cl�
     * @param key > nom de la cl�
     * @param value > valeur de la cl�
     */
    public abstract void setKey(String key, String value);

    /**
     * Changer ou attribuer une valeur d'une cl� (sauvegard� en json)
     * @param key > nom de la cl�
     * @param value > valeur de la cl�
     */
    public abstract void setKey(String key, Object value);

    /**
     * R�cup�rer la valeur d'une cl� en string
     * @param redisInfo
     */
    public abstract void getKey(RedisInfo redisInfo);

    /**
     * R�cup�rer la valeur d'une cl� en l'objet choisi
     * @param redisInfo
     */
    public abstract void getKey(RedisClassInfo redisInfo);

    /**
     * R�cup�rer la valeur d'une cl� en l'objet choisi (avec reflection)
     * @param redisInfo
     */
    public abstract void getKey(RedisTypeInfo redisInfo);

    /**
     * Dire si le RedisCache est connect� au module de connector de l'infrastructure
     * @return
     */
    @Override
    public boolean isConnected() {
        return !(instance instanceof RedisCacheLocal);
    }

}

