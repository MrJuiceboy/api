package fr.hyneria.api.database.caches.redis;

import fr.hyneria.api.database.caches.redis.infos.RedisClassInfo;
import fr.hyneria.api.database.caches.redis.infos.RedisInfo;
import fr.hyneria.api.database.caches.redis.infos.RedisTypeInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * Cette classe permet d'ex�cuter les choses en local du module RedisCache
 * quand l'api n'est pas reli� � l'infrastructure
 * Les m�thodes n'ex�cutent pas vraiment en local le code que vous demander, c'est du FAKE mais cela
 * fonctionnera une fois que votre jeu sera en production
 */
public class RedisCacheLocal extends RedisCache {

    // Cache fake
    public Map<String, Object> fakeCache = new HashMap<>();

    @Override
    public void setKey(String key, String value) {
        fakeCache.put(key, value);
    }

    @Override
    public void setKey(String key, Object value) {
        fakeCache.put(key, value);
    }

    @Override
    public void getKey(RedisInfo redisInfo) {
        redisInfo.call((String) fakeCache.get(redisInfo.getKey()));
    }

    @Override
    public void getKey(RedisClassInfo redisInfo) {
        redisInfo.call((Class) fakeCache.get(redisInfo.getKey()));
    }

    @Override
    public void getKey(RedisTypeInfo redisInfo) {
        redisInfo.call(fakeCache.get(redisInfo.getKey()));
    }

}

