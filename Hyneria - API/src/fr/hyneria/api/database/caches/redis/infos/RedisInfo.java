package fr.hyneria.api.database.caches.redis.infos;

public abstract class RedisInfo {

    private String key;

    public RedisInfo(String key) {
        this.key = key;
    }

    public void call(String value) {
        done(value);
    }

    public abstract void done(String value);

    public String getKey() {
        return key;
    }
}

