package fr.hyneria.api.database.caches.redis.infos;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class RedisClassInfo<T extends Class> extends RedisInfo {

    private Class<T> clazz;

    public RedisClassInfo(String key, Class<T> clazz) {
        super(key);
        this.clazz = clazz;
    }

    public void done(String result) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        call((T) gson.fromJson(result, clazz));
    }

    public abstract void call(Class<T> value);

}
