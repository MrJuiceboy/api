package fr.hyneria.api.database.caches.redis.infos;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class RedisTypeInfo<T> extends RedisInfo {

    private Type type;

    public RedisTypeInfo(String key, Type type) {
        super(key);
        this.type = type;
    }

    public void done(String result) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        call((T) gson.fromJson(result, type));
    }

    public abstract void call(T value);

}
