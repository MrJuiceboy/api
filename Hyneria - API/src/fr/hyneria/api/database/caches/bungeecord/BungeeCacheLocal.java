package fr.hyneria.api.database.caches.bungeecord;

import fr.hyneria.api.database.channels.ChannelMessage;
import fr.hyneria.api.log.LoggerUtils;
import fr.hyneria.api.players.HyneriaPlayer;

/**
 * Cette classe permet d'ex�cuter les choses en local du module BungeeCache
 * quand l'api n'est pas reli� � l'infrastructure
 * Les m�thodes n'ex�cutent pas vraiment en local le code que vous demander, c'est du FAKE mais cela
 * fonctionnera une fois que votre jeu sera en production
 *
 */
public class BungeeCacheLocal extends BungeeCache {

    @Override
    public void sendBungeeExecutorMessage(ChannelMessage channelMessage) {
        LoggerUtils.enableDebug();
        LoggerUtils.debug("Send message to " + channelMessage.getChannelName() + ", execute code of the channel!");
        LoggerUtils.disableDebug();
    }

    @Override
    public void sendBungeeMessage(ChannelMessage channelMessage) {
        LoggerUtils.enableDebug();
        LoggerUtils.debug("Send message to " + channelMessage.getChannelName());
        LoggerUtils.disableDebug();
    }

    @Override
    public void teleport(HyneriaPlayer hyneriaPlayer, String serverName) {
        hyneriaPlayer.get().kickPlayer("T�l�portation � '" + serverName + "'");
    }

    @Override
    public void teleportHub(HyneriaPlayer hyneriaPlayer) {
        hyneriaPlayer.get().kickPlayer("T�l�portation au hub.");
    }

    @Override
    public void sendDisconnectData(HyneriaPlayer hyneriaPlayer) {
        // Fake d�co
        LoggerUtils.logWithColor("�c[DECO] D�connexion de " + hyneriaPlayer.getName());
    }

}

