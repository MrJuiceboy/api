package fr.hyneria.api.database.caches.bungeecord;

import fr.hyneria.api.database.caches.CacheModule;
import fr.hyneria.api.database.channels.ChannelMessage;
import fr.hyneria.api.players.HyneriaPlayer;

public abstract class BungeeCache extends CacheModule {

    // Instance
    public static BungeeCache instance	= new BungeeCacheLocal();

    /**
     * Envoyer un message � tous les bungee pour ex�cuter une instruction de code
     * @param channelMessage
     */
    public abstract void sendBungeeExecutorMessage(ChannelMessage channelMessage);

    /**
     * Envoyer un message � un joueur/groupe de personnes d'un m�me bungee ou d'un autre bungee
     * (utilis� par exemple pour le chat modo)
     * @param channelMessage
     */
    public abstract void sendBungeeMessage(ChannelMessage channelMessage);

    /**
     * T�l�porter un joueur � un nom de serveur sp�cifique
     * @param standPlayer
     * @param serverName
     */
    public abstract void teleport(HyneriaPlayer standPlayer, String serverName);

    /**
     * T�l�porter un joueur au hub
     * @param standPlayer
     */
    public abstract void teleportHub(HyneriaPlayer standPlayer);

    /**
     * Envoi des donn�es de d�connexion � un des bungee pour les traiter et les sauvegarder dans redis
     * @param standPlayer
     */
    public abstract void sendDisconnectData(HyneriaPlayer standPlayer);

    /**
     * Dire si le BungeeCache est connect� au module de connector de l'infrastructure
     * @return
     */
    @Override
    public boolean isConnected() {
        return !(instance instanceof BungeeCacheLocal);
    }

}

