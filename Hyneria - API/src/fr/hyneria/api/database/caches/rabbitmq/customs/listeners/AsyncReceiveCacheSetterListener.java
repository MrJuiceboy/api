package fr.hyneria.api.database.caches.rabbitmq.customs.listeners;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.gson.reflect.TypeToken;
import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.caches.rabbitmq.events.AsyncRabbitEvent;
import fr.hyneria.api.database.caches.rabbitmq.rabbit.PlayerCacheSetter;
import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.Gson;

public class AsyncReceiveCacheSetterListener extends AsyncRabbitEvent {

    public AsyncReceiveCacheSetterListener(HyneriaAPI api) {
        super("cache." + api.getServer().getServerName() + ".set");
        this.setDebug(false);
    }

    @Override
    public void onReceiveMessage(String message) {
        Type collectionType = new TypeToken<PlayerCacheSetter>(){}.getType();
        PlayerCacheSetter cacheInfo = new Gson().fromJson(message, collectionType);
        if (cacheInfo == null) return;
        UUID uuid = cacheInfo.getUniqueId();
        if (!HyneriaPlayer.players.containsKey(uuid)) return;
        Player player = Bukkit.getPlayer(uuid);
        if (player == null) return;
        HyneriaPlayer hyneriaPlayer = HyneriaPlayer.getPlayer(player);
        if (hyneriaPlayer == null) return;
        for (Entry<String, Object> entry : cacheInfo.fields.entrySet()) {
            try {
                Field field = hyneriaPlayer.getClass().getField(entry.getKey());
                if (field == null) continue;
                Object object = entry.getValue();
                if (object instanceof Double) {
                    double variable = (double) object;
                    if ((variable == Math.floor(variable)) && !Double.isInfinite(variable)) {
                        object = (int) variable;
                    }
                }
                field.set(hyneriaPlayer, object);
            }catch(Exception error) {
                error.printStackTrace();
            }
        }
        return;
    }

}

