package fr.hyneria.api.database.caches.rabbitmq.rabbit;

import fr.hyneria.api.database.caches.rabbitmq.rabbit.setters.PlayerCacheSenderInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerCacheSetter extends PlayerCacheSenderInfo {

    public Map<String, Object> fields = new HashMap<>();

    public PlayerCacheSetter(UUID uuid) {
        super(uuid);
    }

}
