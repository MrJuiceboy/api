package fr.hyneria.api.database.caches.rabbitmq;

import fr.hyneria.api.database.caches.CacheModule;
import fr.hyneria.api.database.caches.rabbitmq.events.AsyncRabbitEvent;

public abstract class RabbitCache extends CacheModule {

    public static RabbitCache instance	= new RabbitCacheLocal();

    // Debug
    private static boolean    isDebugged;

    /**
     * Enregistrer un listener d'un packet rabbit
     * @param listener
     */
    public abstract void registerListener(AsyncRabbitEvent listener);

    /**
     * Envoyer un message rabbit dans une queue sans debug
     * @param queue
     * @param message
     */
    public abstract void sendRabbitMessage(String queue, String message);

    /**
     * Encodage par d�faut des messages rabbit
     * @return
     */
    public String getDefaultEncodage() {
        return "UTF8";
    }

    /**
     * R�cup�rer si le debug est activ� ou non lors de l'envoi d'un message rabbitmq
     * @return
     */
    public boolean isDebugged() {
        return isDebugged;
    }

    /**
     * Activer le debug
     */
    public void enableDebug() {
        isDebugged = true;
    }

    /**
     * Changer le statut du debug
     * @param debug
     */
    public void setDebug(boolean debug) {
        isDebugged = debug;
    }

    /**
     * D�sactiver le debug
     */
    public void disableDebug() {
        isDebugged = false;
    }

    /**
     * Dire si le RabbitCache est connect� au module de connector de l'infrastructure
     * @return
     */
    @Override
    public boolean isConnected() {
        return !(instance instanceof RabbitCacheLocal);
    }

}

