package fr.hyneria.api.database.caches.rabbitmq.rabbit.setters;

import fr.hyneria.api.database.caches.rabbitmq.rabbit.PlayerCacheInfo;

import java.util.UUID;

public class PlayerCacheSenderInfo extends PlayerCacheInfo {

    public PlayerCacheSenderInfo(UUID uuid) {
        super(uuid);
    }
}
