package fr.hyneria.api.database.caches.rabbitmq.customs.listeners;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.gson.reflect.TypeToken;
import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.caches.rabbitmq.RabbitCache;
import fr.hyneria.api.database.caches.rabbitmq.events.AsyncRabbitEvent;
import fr.hyneria.api.database.caches.rabbitmq.rabbit.getter.PlayerCacheGetter;
import fr.hyneria.api.database.caches.rabbitmq.rabbit.getter.PlayerCacheGetterResponse;
import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.Gson;


public class AsyncReceiveCacheGetterListener extends AsyncRabbitEvent {

    public AsyncReceiveCacheGetterListener(HyneriaAPI api) {
        super("cache." + api.getServer().getServerName() + ".get");
        this.setDebug(true);
    }

    @Override
    public void onReceiveMessage(String message) {
        Type collectionType = new TypeToken<PlayerCacheGetter>(){}.getType();
        PlayerCacheGetter cacheInfo = new Gson().fromJson(message, collectionType);
        UUID uuid = cacheInfo.getUniqueId();
        if (!HyneriaPlayer.players.containsKey(uuid)) return;
        Player player = Bukkit.getPlayer(uuid);
        if (player == null) return;
        HyneriaPlayer standPlayer = HyneriaPlayer.getPlayer(player);
        if (standPlayer == null) return;
        collectionType = new TypeToken<PlayerCacheGetter>(){}.getType();
        cacheInfo = new Gson().fromJson(message, collectionType);
        Map<String, Object> fields = new HashMap<>();
        for (String fieldName : cacheInfo.fields) {
            try {
                Field field = standPlayer.getClass().getField(fieldName);
                if (field == null) continue;
                String json = new Gson().toJson(field.get(standPlayer));
                String improvement = new Gson().toJson(standPlayer.gadgets);
                System.out.println(fieldName + " >= " + json + " (" + improvement + ")");
                fields.put(fieldName, json);
            }catch(Exception error) {
            }
        }
        PlayerCacheGetterResponse getterResponse = new PlayerCacheGetterResponse(cacheInfo.getUniqueId(), cacheInfo);
        getterResponse.fields = fields;
        RabbitCache.instance.enableDebug();
        RabbitCache.instance.sendRabbitMessage("cache." + getterResponse.id + "." + cacheInfo.cacheId, new Gson().toJson(getterResponse));
    }
}

