package fr.hyneria.api.database.caches.rabbitmq.customs.listeners;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.caches.rabbitmq.events.AsyncRabbitEvent;
import fr.hyneria.api.listeners.PlayerLocalJoinedEvent;
import fr.hyneria.api.log.LoggerUtils;
import fr.hyneria.api.players.HyneriaPlayer;
import fr.hyneria.api.utils.maths.TaskManager;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.GsonBuilder;

public class AsyncLoadPlayerListener extends AsyncRabbitEvent {

    public AsyncLoadPlayerListener(HyneriaAPI api) {
        super("bungeeCord.receive.server." + api.getServer().getServerName());
    }

    @Override
    public void onReceiveMessage(final String message) {
        new Thread() {
            @Override
            public void run() {
                final HyneriaPlayer hyneriaPlayer = new GsonBuilder().create().fromJson(message, HyneriaPlayer.class);
                hyneriaPlayer.players.put(hyneriaPlayer.getUniqueId(), hyneriaPlayer);
                if (hyneriaPlayer == null) {
                    LoggerUtils.warning("Unknown player");
                    return;
                }
                for (int i = 0; i < 5000; i++) {
                    final Player player = Bukkit.getPlayer(hyneriaPlayer.name);
                    if (player != null && player.isOnline()) {
                        hyneriaPlayer.register(player);
                        TaskManager.runTask(new Runnable() {
                            @Override
                            public void run() {
                                PlayerLocalJoinedEvent.sendJoin(player, hyneriaPlayer);
                            }
                        });
                        LoggerUtils.logWithColor(ChatColor.AQUA + "Loaded player " + hyneriaPlayer.name + " (" + HyneriaPlayer.players.size() + " players registereds / " + Bukkit.getOnlinePlayers().size() + " connecteds)");
                        return;
                    }
                    try {
                        sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
        }.start();
    }

}

