package fr.hyneria.api.database.caches.rabbitmq;

import fr.hyneria.api.database.caches.rabbitmq.events.AsyncRabbitEvent;
import fr.hyneria.api.log.LoggerUtils;

/**
 * Cette classe permet d'ex�cuter les choses en local du module RabbitCache
 * quand l'api n'est pas reli� � l'infrastructure
 * Les m�thodes n'ex�cutent pas vraiment en local le code que vous demander, c'est du FAKE mais cela
 * fonctionnera une fois que votre jeu sera en devroom/production
 * @author xMalware
 */
public class RabbitCacheLocal extends RabbitCache {

    @Override
    public void sendRabbitMessage(String queue, String message) {
        LoggerUtils.enableDebug();
        LoggerUtils.debug("[FAKE-RABBIT] Send rabbit message to " + queue + " (" + message + " / DEBUG " + (isDebugged() ? "ON" : "OFF") + " / " + getDefaultEncodage() + ")");
        LoggerUtils.disableDebug();
    }

    @Override
    public void registerListener(AsyncRabbitEvent listener) {
        LoggerUtils.enableDebug();
        LoggerUtils.debug("[FAKE-RABBIT] Loaded listener " + listener.toString().split("@")[0]);
        LoggerUtils.disableDebug();
    }

}
