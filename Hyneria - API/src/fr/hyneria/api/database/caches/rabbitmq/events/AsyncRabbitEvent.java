package fr.hyneria.api.database.caches.rabbitmq.events;

public abstract class AsyncRabbitEvent {

    // Fields
    private String queue;
    private boolean debug;

    /**
     * Nouvelle queue � �couter
     * @param queue
     */
    public AsyncRabbitEvent(String queue) {
        this.queue = queue;
    }

    /**
     * R�cup�rer le nom de la queue
     * @return
     */
    public String getQueue() {
        return this.queue;
    }

    /**
     * Activer/d�sactiver le d�bug lors d'un nouveau message re�u
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Indique si le debug a �t� activ� ou non
     * @return
     */
    public boolean isDebugged() {
        return this.debug;
    }

    /**
     * M�thode appel�e lors d'un nouveau messge dans la queue
     * @param message
     */
    public abstract void onReceiveMessage(String message);

}

