package fr.hyneria.api.database.caches.rabbitmq.rabbit;

import java.util.UUID;

public class PlayerCacheInfo {

    private UUID uuid;

    public PlayerCacheInfo(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUniqueId() {
        return this.uuid;
    }
}
