package fr.hyneria.api.database.caches.rabbitmq.rabbit.getter;

import fr.hyneria.api.database.caches.rabbitmq.rabbit.setters.PlayerCacheSenderInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerCacheGetter extends PlayerCacheSenderInfo {

    private static int autoIncrementer = -1;

    public int id = 0;
    public UUID cacheId;
    public List<String> fields = new ArrayList<>();

    public PlayerCacheGetter(UUID uuid) {
        super(uuid);
        this.cacheId = UUID.randomUUID();
        this.id++;
    }
}