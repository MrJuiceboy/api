package fr.hyneria.api.database.caches.rabbitmq.rabbit.getter;

import fr.hyneria.api.database.caches.rabbitmq.rabbit.setters.PlayerCacheSenderInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerCacheGetterResponse extends PlayerCacheSenderInfo {

    public Map<String, Object> fields = new HashMap<>();

    public int id;

    public PlayerCacheGetterResponse(UUID uuid, PlayerCacheGetter cacheGetter) {
        super(uuid);
        this.id = cacheGetter.id;
    }

}

