package fr.hyneria.api.database.caches;

import fr.hyneria.api.listeners.HyneriaListener;

public abstract class CacheModule extends HyneriaListener {
	
	public abstract boolean isConnected();
	
}
