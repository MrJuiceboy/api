package fr.hyneria.api.database.channels;

public class ChannelMessage {

    // Fields
    private ChannelType channelType;
    private String      channelName;
    private String      message;
    private String      lore;

    /**
     * Constructeur d'un channel message
     * @param channelType > type du canal
     * @param channelName > nom du canal pour �tre intercept� et class�
     * @param message > message du canal
     * @param lore > lore du message
     */
    public ChannelMessage (ChannelType channelType, String channelName, String message, String lore) {
        this.channelType = channelType;
        this.channelName = channelName;
        this.message     = message;
        this.lore        = lore;
    }

    /**
     * R�cup�rer le type du channel
     * @return
     */
    public ChannelType getChannelType() {
        return this.channelType;
    }

    /**
     * R�cup�rer le nom du channel
     * @return
     */
    public String getChannelName() {
        return this.channelName;
    }

    /**
     * R�cup�rer le message du channel
     * @return
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * R�cup�rer le lore du channel
     * @return
     */
    public String getLore() {
        return this.lore;
    }

    /**
     * �num�ration du type du message
     * @author xMalware
     */
    public enum ChannelType {
        PLAYER,
        GROUP;
    }
}