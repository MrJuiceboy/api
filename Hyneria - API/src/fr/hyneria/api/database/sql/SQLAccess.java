package fr.hyneria.api.database.sql;

import java.sql.*;

import org.bukkit.Bukkit;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.entity.Player;

public class SQLAccess {

	private String urlbase,host,database,user,pass;
	public Connection connection;

	public SQLAccess(HyneriaAPI pl, String urlbase, String host, String database, String user, String pass){
		this.urlbase = urlbase;
		this.host = host;
		this.database = database;
		this.user = user;
		this.pass = pass;
	}
	
	public void connection(){
		if(!isConnected()){
			try {
				connection = DriverManager.getConnection(urlbase + host + "/" + database, user, pass);
				System.out.println("API connected ok");
				Bukkit.getScheduler().scheduleSyncRepeatingTask(HyneriaAPI.getInstance(), new Runnable(){
			        public void run(){
			        	try {
			        		System.out.println("API refresh connected ok");
			        		connection = DriverManager.getConnection(urlbase + host + "/" + database, user, pass);
						} catch (SQLException e) {
							e.printStackTrace();
						}
			        }
			    }, 500000, 500000);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isConnected(){
		return connection != null;
	}
	
	/* (la réaction envers la BDD)
	 * 
	 * INSERT
	 * UPDATE
	 * DELETE
	 * SELECT
	 * 
	 * PREPARER ?,?
	 * REMPLACER les ? PAR DES VALEURS
	 * EXECUTE
	 * 
	 */	 
	
	public void createAccount(Player player){
		if(!hasAccount(player)){			
			try {
				PreparedStatement q = connection.prepareStatement("INSERT INTO players (uuid,username,server,rank,hycoins,supercoins) VALUES (?,?,?,?,?,?)");
				q.setString(1, player.getUniqueId().toString());
				q.setString(2, player.getName());
				q.setString(3, Bukkit.getServer().getServerName().toString());
				q.setInt(4, 0);
				q.setInt(5, 0);
				q.setInt(6, 0);
				q.execute();
				q.close();
			} catch (SQLException e) {
				e.printStackTrace();
				}
			}
	}
	
	public boolean hasAccount(Player player){
		try {
			PreparedStatement q = connection.prepareStatement("SELECT uuid FROM players WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet resultat = q.executeQuery();
			boolean hasAccount = resultat.next();
			q.close();
			return hasAccount;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
