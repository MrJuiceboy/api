package fr.hyneria.api.database.redis;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;

public class RedisAccess {

	public static RedisAccess instance;
	private RedissonClient redissonclient;

	public RedisAccess(RedisCredentials redisCredentials) {
		instance = this;
	}

	public static void init() {
		new RedisAccess(new RedisCredentials("127.0.0.1", "8y9kR2GbLM7U9x6T4gd76P797vAYYyv3vNhjzGfQg5t26aXZ6T", 6379));
	}

	public static void close() {
		RedisAccess.instance.getRedissonclient().shutdown();
	}

	public RedissonClient initRedisson(RedisCredentials redisCredentials) {
		final Config config = new Config();

		config.setCodec(new JsonJacksonCodec());
		config.setThreads(8);
		config.setNettyThreads(8);
		config.useSingleServer()
				.setAddress(redisCredentials.toRedisURL())
				.setPassword(redisCredentials.getPassword())
				.setDatabase(1)
				.setClientName(redisCredentials.getClientName());
		return Redisson.create(config);
	}

	public RedissonClient getRedissonclient() {
		return redissonclient;
	}
}