package fr.hyneria.api.database.exceptions;

import java.util.UUID;

@SuppressWarnings("serial")
public class AccountNotFoundException extends Exception  {
	
	public AccountNotFoundException(UUID uuid) {
		super("The account (+" + uuid.toString() + ") is not found");
	}

}
