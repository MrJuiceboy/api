package fr.hyneria.api.log;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

public class LoggerUtils {

	public static boolean	debug	= false;
	/**
	 * @param message
	 */
	private static void OpMessage(String message) {
		for (OfflinePlayer p : Bukkit.getOperators())
			if (p.isOnline()) p.getPlayer().sendMessage(message);
	}
	/**
	 * Log en prenant en compte les couleurs et sans classer le message
	 * @param string
	 */
	public static void logWithColor(String string) {
		Bukkit.getServer().getConsoleSender().sendMessage(string);
	}

	/**
	 * Log uniquement si le debug est actif spamOp true
	 * @param string
	 */
	public static void debug(String string) {
		if (!debug) return;
		debug(string, true);
	}
	/**
	 * Log uniquement si le debug est actif
	 * @param string
	 * @param spamOp
	 */
	public static void debug(String string, boolean spamOp) {
		if (!debug) return;
		String prefix = "�5[Debug]�r";
		Bukkit.getServer().getConsoleSender().sendMessage(prefix + " " + string);
	}
	/**
	 * 
	 */
	public static void warning(String string) {
		String prefix = "�4[WARNING]�r";
		Bukkit.getServer().getConsoleSender().sendMessage(prefix + " " + string);
		OpMessage(prefix + " " + string);
	}

	/**
	 * Log en Info
	 * @param string
	 */
	public static void info(String string) {
		String prefix = "�4[INFO]�r";
		Bukkit.getServer().getConsoleSender().sendMessage(prefix + " " + string);
	}
	
	public static void enableDebug() {
		debug = true;
	}
	
	public static void disableDebug() {
		debug = false;
	}

}
