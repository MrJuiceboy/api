package fr.hyneria.api;

import fr.hyneria.api.database.manager.HyneriaPlayerJoinListener;
import fr.hyneria.api.games.manager.listeners.PlayerConnectionListener;
import fr.hyneria.api.listeners.PlayerLocalJoinedEvent;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.hyneria.api.commands.GCCommand;
import fr.hyneria.api.commands.ModulesCommand;
import fr.hyneria.api.commands.PluginsCommand;
import fr.hyneria.api.database.redis.RedisAccess;
import fr.hyneria.api.database.sql.SQLAccess;
import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.AntiTabInfoListener;
import fr.hyneria.api.listeners.ChatSpamListener;
import fr.hyneria.api.listeners.CommandSpamListener;
import fr.hyneria.api.opti.WorldFileSave;
import fr.hyneria.api.packets.PacketManager;

public class HyneriaAPI extends JavaPlugin {

    /*
     * FIELD
     */

    private static HyneriaAPI instance;
    public PluginManager pm;
    public ConsoleCommandSender console;
    private PacketManager packetManager;
    public static SQLAccess sql;
    
    @Override
    public void onLoad() {
        instance = this;
    }

    public void onEnable() {
        registerFields();
        registerCommands();
        registerListeners();
        registerOptimisations();
        GameManager.newInstance();
        
        RedisAccess.init();
        sql = new SQLAccess(this, "jdbc:mysql://","217.182.168.154", "hyneria-server", "warfight", "Xk25b4BwT");
        sql.connection();
    }

    /**
     * Modifier les fields principaux
     */
    public void registerFields() {
        console = getServer().getConsoleSender();
        pm = getServer().getPluginManager();
        packetManager = new PacketManager();
    }

    /**
     * Enregistrer les optimisations
     */
    public void registerOptimisations() {
        new WorldFileSave();
    }

    /**
     * Enregistrer les commandes
     */
    public void registerCommands() {
        new GCCommand().register();
        getCommand("pl").setExecutor(new PluginsCommand());
        getCommand("modules").setExecutor(new ModulesCommand());
    }

    /**
     * Enregistrer les listeners
     * @param manager
     */
    public void registerListeners() {
        GameManager manager = GameManager.getInstance();
        //manager.registerListener(new AntiTabInfoListener(), this);
        manager.registerListener(new ChatSpamListener(), this);
        manager.registerListener(new CommandSpamListener(), this);
        manager.registerListener(new HyneriaPlayerJoinListener(), this);
        manager.registerListener(new PlayerConnectionListener(), this);
        manager.registerListener(new PlayerLocalJoinedEvent(), this);
    }

    /**
     * Getting the packet manager
     * @return
     */
    public PacketManager getPacketManager() {
        return this.packetManager;
    }

    /**
     * Getting the map name
     * @param localMap > if the server is running on local server, it's cannot find the map, please define a good map name in this parameter
     * @return the name of the map
     */
    public String getMapName(String localMap) {
        if (GameServer.getInstance().isConnected()) return GameServer.getInstance().getMap();
        else return localMap;
    }

    /**
     * Enlever les unload de chunks
     */
    public void removeWorldSave() {
        for (World world : Bukkit.getWorlds()) {
            world.setAutoSave(false);
            world.setKeepSpawnInMemory(true);
        }
    }

    public static HyneriaAPI getInstance() {
        return instance;
    }

    @Override
    public void onDisable() {
        // Cancelling all tasks
        this.getServer().getScheduler().cancelTasks(this);
        RedisAccess.close();
    }
}