package fr.hyneria.api.punishments;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

public class Punishment {
	
	@Expose public PunishmentType punishmentType;
	@Expose public long		      timestamp;
	@Expose public long		      expire;
	@Expose public String		  reason;
	@Expose public List<ChangePunishment>   changes;
	
	/**
	 * Constructeur
	 * @param reason
	 * @param punishmentType
	 * @param expire
	 * @param timestamp
	 */
	public Punishment (String reason, PunishmentType punishmentType, long timestamp, long expire) {
		this.reason = reason;
		this.punishmentType = punishmentType;
		this.timestamp = timestamp;
		this.expire = expire;
		this.changes = new ArrayList<ChangePunishment>();
	}
	
	/**
	 * R�cup�rer la raison
	 * @return
	 */
	public String getReason() {
		return this.reason;
	}
	
	/**
	 * R�cup�rer le type de la punition
	 * @return
	 */
	public PunishmentType getType() {
		return this.punishmentType;
	}
	
	/**
	 * R�cup�rer le timestamp
	 * @return
	 */
	public long getTimestamp() {
		return this.timestamp;
	}
	
	/**
	 * R�cup�rer le temps d'expiration
	 * @return
	 */
	public long getExpireTime() {
		return this.expire;
	}
	
	/**
	 * If this is expired
	 * @return
	 */
	public boolean isExpired() {
		return System.currentTimeMillis() > this.getExpireTime();
	}
	
	/**
	 * If the punishment is canncelled
	 * @return
	 */
	public boolean isCancelled() {
		for (ChangePunishment change : changes)
			if (change.isPunishmentCancelled()) return true;
		return false;
	}
	
	/**
	 * Ajouter une punition grace � une autre
	 * @param punishment
	 */
	public void addFrom(String nickname, String reason, long expireTime, PunishmentType type, boolean cancellation) {
		if (!this.getType().equals(type)) return;
		if (!this.getType().canBeAddUp()) return;
		if (cancellation) {
			changes.add(new ChangePunishment(nickname, reason, cancellation));
		}else{
			long toBeAdded = expireTime - System.currentTimeMillis();
			this.expire += toBeAdded;
			changes.add(new ChangePunishment(nickname, "Ajout d'un temps (" + reason + ")", false));
		}
	}
	
}
