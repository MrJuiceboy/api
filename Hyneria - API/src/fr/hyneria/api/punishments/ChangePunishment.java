package fr.hyneria.api.punishments;

import com.google.gson.annotations.Expose;

public class ChangePunishment {

	@Expose String  nickname;
	@Expose String  reason;
	@Expose long    time;
	@Expose boolean cancellation;
	
	/**
	 * Constructor
	 * @param nickname
	 * @param reason
	 */
	public ChangePunishment(String nickname, String reason, boolean cancellation) {
		this.nickname = nickname;
		this.reason = reason;
		this.time = System.currentTimeMillis();
		this.cancellation = cancellation;
	}
	
	/**
	 * Getting the nickname
	 * @return
	 */
	public String getNickname() {
		return this.nickname;
	}
	
	/**
	 * Getting the reason
	 * @return
	 */
	public String getReason() {
		return this.reason;
	}
	
	/**
	 * Getting the time
	 * @return
	 */
	public long getTimestamp() {
		return this.time;
	}
	
	/**
	 * Getting the cancellation
	 * @return
	 */
	public boolean isPunishmentCancelled() {
		return this.cancellation;
	}
	
}
