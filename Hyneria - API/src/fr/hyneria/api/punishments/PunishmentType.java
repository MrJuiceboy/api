package fr.hyneria.api.punishments;

public enum PunishmentType {
	
	BAN(true),
	KICK(false),
	MUTE(true),
	AVERT(false);
	
	private boolean addUp;
	
	private PunishmentType(boolean addUp) {
		this.addUp = addUp;
	}
	
	public boolean canBeAddUp() {
		return addUp;
	}
	
}
