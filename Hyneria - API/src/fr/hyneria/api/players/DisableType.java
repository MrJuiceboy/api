package fr.hyneria.api.players;

public enum DisableType {
	DISABLED,
	ENABLED,
	ONLY_FOR_FRIENDS;
}