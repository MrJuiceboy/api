package fr.hyneria.api.players.ranks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.hyneria.api.players.PlayerFields;

public class Rank {
	
	// STATIC FIELDS
	private static Map<String, Rank> ranks = new HashMap<String, Rank>();
	
	//public  static Map<Integer, VIP> vips = new HashMap<>();
	
	
	static {
		new AdminRank();
	}
	
	// PRIVATE FIELDS
	private int 		 opLevel;
	private String  	 prefix;
	private List<String> bungeePermissions;
	private List<String> bukkitPermissions;
	private String		 dbName;
	protected boolean    isOp;
	
	/**
	 * Constructor of a new rank
	 * @param opLevel
	 * @param prefix
	 * @param bungeePermissions
	 * @param bukkitPermissions
	 * @param dbName
	 */
	@SuppressWarnings("unchecked")
	public Rank (int opLevel, String prefix, List<String> bungeePermissions, List<String> bukkitPermissions, String dbName) {
		this.opLevel = opLevel;
		this.prefix  = prefix;
		this.bungeePermissions = (List<String>) (bungeePermissions == null ? new ArrayList<Object>() : bungeePermissions);
		this.bukkitPermissions = (List<String>) (bukkitPermissions == null ? new ArrayList<Object>() : bukkitPermissions);
		this.dbName = dbName;
		this.register();
	}
	
	/**
	 * Register the rank in memory
	 */
	public void register() {
		ranks.put(this.getDBName(), this);
	}
	
	/**
	 * Getting the op level of this rank
	 * @return
	 */
	public int getOpLevel() {
		return this.opLevel;
	}
	
	/**
	 * Getting the prefix of this rank
	 * @return
	 */
	public String getPrefix() {
		return this.prefix;
	}
	
	/**
	 * Getting the list of all bungee permissions of this rank
	 * @return
	 */
	public List<String> getBungeePermissions() {
		return this.bungeePermissions;
	}
	
	/**
	 * Getting the list of all bukkit permissions of this rank
	 * @return
	 */
	public List<String> getBukkitPermissions() {
		return this.bukkitPermissions;
	}
	
	/**
	 * Return if the rank contains a permission or not (with two lists of bukkit and bungee permissions!)
	 * @param permission
	 * @return
	 */
	public boolean hasPermission(String permission) {
		return this.bungeePermissions.contains(permission) || this.bukkitPermissions.contains(permission);
	}
	
	/**
	 * Return if the rank contains a bungeecord permission or not
	 * @param permission
	 * @return
	 */
	public boolean hasBungeePermission(String permission) {
		return this.bungeePermissions.contains(permission);
	}
	
	/**
	 * Return if the rank contains a bukkit permission or not
	 * @param permission
	 * @return
	 */
	public boolean hasBukkitPermission(String permission) {
		return this.bukkitPermissions.contains(permission);
	}
	
	/**
	 * Getting the not formatted name of the rank
	 * @return
	 */
	public String getDBName() {
		return this.dbName;
	}
	
	/**
	 * Getting if this is op
	 * @return
	 */
	public boolean isOp() {
		return this.isOp;
	}
	
	// STATIC METHODS
	
	/**
	 * Return the rank of the database name of a rank
	 * @param name
	 * @return
	 */
	public static Rank getRank(String name) {
		return ranks.get(name);
	}
	
	/**
	 * Return the rank of the power of a rank
	 * @param name
	 * @return
	 */
	public static Rank getRank(int power) {
		for (Rank rank : ranks.values())
			if (rank.getOpLevel() == power) return rank;
		return null;
	}
	
	/**
	 * Return the rank of a player
	 * @param name
	 * @return
	 */
	public static Rank getRank(PlayerFields playerFields) {
		for (Rank rank : ranks.values())
			if (rank.getOpLevel() == playerFields.opLevel) return rank;
		return null;
	}
	
	/**
	 * Detect if a rank exists with a specific database name
	 * @param name
	 * @return
	 */
	public static boolean contains(String name) {
		return getRank(name) != null;
	}
	
}
