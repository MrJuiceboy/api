package fr.hyneria.api.players.ranks;

import java.util.Arrays;

public class AdminRank extends Rank {

	public AdminRank() {
		super(100, "�4[Admin] ", Arrays.asList("hyneria.nick", "hyneria.setrank", "hyneria.hycoins", "hyneria.supercoins", "hyneria.send",
		"hyneria.alert", "hyneria.connect", "hyneria.spy", "hyneria.bypass.maintenance", "hyneria.channels.moderation", "hyneria.channels.moderation.governor",
		"hyneria.punishments.auto", "hyneria.punishments.ban", "hyneria.punishments.avertissement", "hyneria.punishments.kick", "hyneria.punishments.mute",
		"hyneria.punishments.unban", "hyneria.punishments.unmute"), Arrays.asList("governor.admin", "hyneria.addevent", "hub.locate", "hyneria.removeevent", "hyneria.hub.tp", "hyneria.hub.tp.others", "bukkit.*", "bukkit.command.*", "minecraft.*", "minecraft.command.*", "minecraft.command.gamemode",
				"bukkit.command.gamemode", "hyneria.staffroomaccess", "hyneria.join", "hyneria.tablepunishmentsaccess", "hyneria.tablepunitions.game",
				"hyneria.tablepunitions.chat", "hyneria.locate", "hyneria.tp", "hyneria.tp.others", "hub.slowmode", "hub.slowmode.maxbetween.30", "hub.slowmode.maxbetween.120", "hub.slowmode.maxbetween.300",
				"hub.slowmode.maxtime.1800", "hub.slowmode.maxtime.3600", "hub.slowmode.maxtime.43200", "hub.slowmode.maxtime.86400", "hub.bypassslowmode", "hyneria.broadcast"), "Administrateur");
		this.isOp = true;
	}	
}
