package fr.hyneria.api.players;

import java.util.Map;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.database.caches.bungeecord.BungeeCache;
import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.players.ranks.Rank;
import fr.hyneria.api.plib.MessagePacket;

public class PlayerCache {
	
	static String prefix = "[FAKE-CACHE] §6";
	private HyneriaPlayer hyneriaPlayer;
	
	PlayerCache(HyneriaPlayer standPlayer) {
		this.hyneriaPlayer = standPlayer;
	}
	
	/**
	 * Récupérer le paramètre d'une clé dans le jeu choisi
	 * @param game > JEU
	 * @param key  > CLÉ
	 * @return VALEUR
	 */
	public String getSetting(HyneriaPlayer game, String key) {
		Map<String, String> settings = hyneriaPlayer.settings.get(game.getName());
		return settings.containsKey(key) ? settings.get(key) : null;
	}
	
	/**
	 * Attribuer la valeur d'une clé si elle n'existe pas dans le jeu choisi
	 * @param game > JEU
	 * @param key  > CLÉ
	 * @param value > VALEUR
	 */
	public void setDefaultSetting(HyneriaPlayer game, String key, String value) {
		Map<String, String> settings = hyneriaPlayer.settings.get(game.getName());
		if (settings.containsKey(key)) return;
		settings.put(key, value);
	}
	
	/**
	 * Changer la valeur d'une clé dans le jeu choisi
	 * @param game > JEU
	 * @param key  > CLÉ
	 * @param value > VALEUR
	 */
	public void setSetting(HyneriaPlayer game, String key, String value) {
		Map<String, String> settings = hyneriaPlayer.settings.get(game.getName());
		settings.put(key, value);
	}
	
	/**
	 * Récupérer le niveau d'un kit/amélioration/achat/possession/... d'un joueur
	 * @param improvement > La possession
	 * @return > Les données de la possession de ce qui a été demandé d'un joueur
	 */
	//SOON
	
	/**
	 * Récupérer le niveau d'une statistique d'un joueur
	 * @param statistic > La statistique
	 * @return > Les données de la statistique d'un joueur
	 */
	//SOON

	
	/**
	 * Envoyer au joueur le message qu'il a gagné ses gains et son nombre (coins & points vip)
	 */
	public void sendTotalGains() {
		// Envoi des coins et points vip à la fin de l'instance/déconnexion
		if (hyneriaPlayer.tempHyCoins > hyneriaPlayer.hycoins || hyneriaPlayer.tempSuperCoins > hyneriaPlayer.supercoins) {
			// Mettre à jour les coins
			String liner = GameServer.bilip;
			// Envoi d'un liner
			double difCoins = hyneriaPlayer.tempHyCoins - hyneriaPlayer.hycoins;
			double difVipPoints = hyneriaPlayer.tempSuperCoins - hyneriaPlayer.supercoins;
			// Envoi du message
			hyneriaPlayer.sendMessage(liner + " §e♦ Gains ♦ " + liner, MessagePacket.CHAT);
			hyneriaPlayer.sendMessage(liner + " §9Coins: §6" + difCoins + " §9gagné" + (difCoins > 1 ? "s" : ""), MessagePacket.CHAT);
			hyneriaPlayer.sendMessage(liner + " §9Points VIP: §3" + difVipPoints + " §9gagné" + (difVipPoints > 1 ? "s" : ""), MessagePacket.CHAT);
		}
	}
	
	/**
	 * Récupérer le nombre de points vip gagnés dans la partie actuelle
	 * @return
	 */
	public double getCacheEarnedCoins() {
		return hyneriaPlayer.tempHyCoins - hyneriaPlayer.getHycoins();
	}
	
	/**
	 * Récupérer le nombre de points vip gagnés dans la partie actuelle
	 * @return
	 */
	public double getCacheEarnedVipPoints() {
		return hyneriaPlayer.tempSuperCoins - hyneriaPlayer.getSupercoins();
	}

	/**
	 * Téléporter le joueur à un autre serveur
	 * @param serverName
	 */
	public void teleport(String serverName) {
		BungeeCache.instance.teleport(this.hyneriaPlayer, serverName);
	}
	
	/**
	 * Téléporter le joueur au hub
	 */
	//SOON
	
	/**
	 * Récupérer si le joueur a une permission spécifique
	 * @param permission
	 * @return
	 */
	public boolean hasPermission(String permission) {
		Rank rank = Rank.getRank(hyneriaPlayer);
		if (rank == null) return this.hyneriaPlayer.get().hasPermission(permission);
		return rank.hasBukkitPermission(permission) || rank.hasBungeePermission(permission);
	}
	
	/**
	 * Ajouter la permission en cache au joueur
	 * @param permission
	 */
	public void addPermission(String permission) {
        this.hyneriaPlayer.get().addAttachment(HyneriaAPI.getInstance(), permission, true);
	}
	
	/**
	 * Retirer la permission en cache au joueur
	 * @param permission
	 */
	public void removePermission(String permission) {
        this.hyneriaPlayer.get().addAttachment(HyneriaAPI.getInstance(), permission, false);
	}
	
	//AddHyCoins
	
	//AddSuperCoins
	
	/**
	 * Récupérer le nom réel du joueur (no /nick)
	 * @return
	 */
	public String getRealName() {
		return this.hyneriaPlayer.realName;
	}
	
}
