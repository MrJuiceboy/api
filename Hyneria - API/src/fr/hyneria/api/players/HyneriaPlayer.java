package fr.hyneria.api.players;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.NullArgumentException;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.log.LoggerUtils;
import fr.hyneria.api.players.exceptions.NoLoadedPlayerException;
import fr.hyneria.api.players.ranks.Rank;
import fr.hyneria.api.plib.MessagePacket;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;

public class HyneriaPlayer extends PlayerFields {
	
	// Players
	public final static Map<UUID, HyneriaPlayer> players = new HashMap<UUID, HyneriaPlayer>();
	private PlayerCache playerCache;
	public  int 		tempOpLevel;
	
	/**
	 * Constructor
	 */
	public HyneriaPlayer(Player player) {
		this.uuid = player.getUniqueId();
		this.name = player.getName();
		this.register(player);
	}
	
	public boolean hasNickname() {
		return !this.name.equals(this.realName);
	}
	
	public void register(Player player) {
		tempOpLevel = this.opLevel;
		if (hasNickname()) {
			this.opLevel = 0;
			LoggerUtils.info("[NICK] Nickname is " + this.name + " instead of " + this.realName + ".");
			if (this.vipLevel > 0) {
				//VIP vip = VIP.getVipLevel(this);
				//this.fakePrefix = vip.getPrefix();
			}
		}
		players.put(this.getUniqueId(), this);
		if (this.playerCache == null) this.playerCache = new PlayerCache(this);
		tempHyCoins = hycoins;
		tempSuperCoins = supercoins;
		// Préparation
		//Load stats
		
		// Ajout des permissions
		Rank rank = Rank.getRank(this);
		// Aucun rang
		if (rank == null) return;
		// Ajout des permissions
		for (String permissions : rank.getBukkitPermissions())
			player.addAttachment(HyneriaAPI.getInstance(), permissions, true);
		if (rank.isOp()) player.setOp(true);
	}
	
	public Player get() {
		return HyneriaAPI.getInstance().getServer().getPlayer(this.getUniqueId());
	}
	
	public void sendMessage(String message, MessagePacket type) throws NullArgumentException {
		if (type == null) // Null type
			throw new NullArgumentException("The type of the message can't be null!");
		if (message == null) // Null message
			throw new NullArgumentException("The message can't be null!");
		Player player = HyneriaAPI.getInstance().getServer().getPlayer(this.uuid);
		if (player == null || !player.isOnline()) return; // Not online player
		IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
		PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(ppoc);
	}

	/**
	 * Récupérer le cache du joueur
	 * @return
	 */
	public PlayerCache getCache() {
		return this.playerCache;
	}

	/**
	 * Do not use
	 * @param hycoins
	 */
	public void subtractHyCoins(double hycoins) {
		this.tempHyCoins -= hycoins;
	}

	/**
	 * Do not use
	 * @param supercoins
	 */
	public void subtractSuperCoins(double supercoins) {
		this.tempSuperCoins -= supercoins;
	}
	
	/**
	 * Getting the player
	 * @return
	 */
	public static HyneriaPlayer getPlayer(Player player) {
		if (!players.containsKey(player.getUniqueId())) throw new NoLoadedPlayerException(player.getName());
		return players.get(player.getUniqueId());
	}
	
	/**
	 * @deprecated do not use
	 * @param player
	 * @return 
	 */
	public static HyneriaPlayer createPlayer(Player player) {
		return new HyneriaPlayer(player);
	}
}
