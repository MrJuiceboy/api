package fr.hyneria.api.players.exceptions;

@SuppressWarnings("serial")
public class NoLoadedPlayerException extends RuntimeException {
	
	public NoLoadedPlayerException(String player) {
		super("The player " + player + " is not loaded ! Please add check before get the HyneriaPlayer !");
	}
	
}
