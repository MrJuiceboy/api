package fr.hyneria.api.players;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.google.gson.annotations.Expose;

import fr.hyneria.api.players.ranks.Rank;
import fr.hyneria.api.punishments.Punishment;

public class PlayerFields {
	
	// Fields
	
	// Ces fields sont prédéfinies à la connexion, ils ne chargent pas
	// directement à partir
	// de l'api
	
	@Expose public String								realName;
	@Expose public boolean								isLoaded;
	@Expose public UUID									uuid				= null;
	@Expose public String								name				= null;
	@Expose public long									firstConnection;
	@Expose public long									lastConnection		= System.currentTimeMillis() / 1000L;
	@Expose public int									opLevel				= 0;
	@Expose public int									vipLevel			= 0;
	@Expose public double								hycoins				= 100;
	public double										tempHyCoins			= 0;
	public double										tempSuperCoins		= 0;
	@Expose public double								supercoins			= 100;
	@Expose public boolean								hidePlayers			= false;
	@Expose public boolean								isOnline			= true;
	@Expose public DisableType							disableMP			= DisableType.ENABLED;
	@Expose public DisableType							disableParty		= DisableType.ENABLED;
	@Expose public DisableType							disableFriends		= DisableType.ENABLED;
	@Expose public DisableType							disableHubChat		= DisableType.ENABLED;
	@Expose public String								function			= "Joueur";
	@Expose public int									dataOrder			= 0;
	@Expose public Map<String, Long>					playedTime			= new HashMap<String, Long>();
	@Expose public Collection<String>					gadgets				= new ArrayList<String>();
	public String										fakePrefix			= "";
	public long											tempLastMessageTime	= 0L;
	public String										tempLastMessage		= "";
	public long											tempLastCommand		= 0;
	public long											tempLoginTimestamp  = 0;
	@Expose public String								hubLocation			= "";
	@Expose public Map<String, Long>					improvements		= new HashMap<String, Long>();
	@Expose public Map<String, Long>				    statistics			= new HashMap<String, Long>();
	@Expose public Collection<String>					logs				= new ArrayList<String>();
	
	// Systéme de reconnexion
	@Expose public String								lastDisconnectedServer;
	@Expose public long									lastDisconnectedServerTime;
	@Expose public boolean								reconnect;
	
	// Party
	@Expose public List<UUID>							party				= new ArrayList<UUID>();
	
	// Settings
	// (Game) (Key, Value)
	@Expose public Map<String	, Map<String, String>>	settings			= new HashMap<String, Map<String, String>>();
	
	@Expose public Map<Long, Punishment>				punishments 		= new HashMap<Long, Punishment>();
	
	/**
	 * Getting the unique id
	 * 
	 * @return
	 */
	public UUID getUniqueId() {
		return this.uuid;
	}
	
	/**
	 * Getting the nickname
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Getting the prefix
	 * @return
	 */
	public String getPrefix() {
		if (fakePrefix != null && !"".equals(fakePrefix)) return fakePrefix;
		Rank rank = Rank.getRank(this);
		if (rank == null) return "§7";
		else return rank.getPrefix();
	}
	
	public double getSupercoins() {
		return this.tempSuperCoins;
	}
	
	public double getHycoins() {
		return this.tempHyCoins;
	}
	
	/*public VIP getVipLevel() {
		return VIP.getVipLevel(this);
	}*/
	
}
