package fr.hyneria.api.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.games.manager.server.SManager;

public class GCCommand extends AbstractCommand {
	
	static List<String> aliases = new ArrayList<String>();
	
	static {
		aliases.add("garbagecollector");
		aliases.add("freeze");
		aliases.add("memory");
		aliases.add("ping");
		aliases.add("tps");
		aliases.add("tickspersecond");
		aliases.add("ticksperseconds");
		aliases.add("lag");
		aliases.add("bug");
	}
	
	public GCCommand() {
		super("gc", "/<command>", "", aliases);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		sender.sendMessage("§e---------------------------");
		// TPS GESTION
		sender.sendMessage("§e> §bServeur: " + Bukkit.getServerName());
		sender.addAttachment(HyneriaAPI.getInstance(), "bukkit.command.tps", true, 1);
		Bukkit.dispatchCommand(sender, "tps");
		// PING gestion
		if (sender instanceof Player)
			sender.sendMessage("§6Ping: " + ((CraftPlayer)((Player)sender)).getHandle().ping + "ms");
		sender.sendMessage("§e> §e---------------------------");
		SManager.getInstance().executeLagChecker();
		return true;
	}
	
}
