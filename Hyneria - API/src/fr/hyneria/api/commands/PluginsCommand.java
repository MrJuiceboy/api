package fr.hyneria.api.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.games.manager.GameManager;
import fr.hyneria.api.listeners.HyneriaListener;

public class PluginsCommand extends HyneriaListener implements CommandExecutor {
	
	public PluginsCommand() {
		GameManager.getInstance().registerListener(HyneriaAPI.getInstance(), this);
	}
	
	@EventHandler
	public void onRemote(PlayerCommandPreprocessEvent event) {
		event.setMessage(event.getMessage().replaceAll("/plugins", "/pl"));
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pl")) {
			GameServer.getInstance().callPluginsList(sender);
			return true;
		}
		return false;
	}
}
