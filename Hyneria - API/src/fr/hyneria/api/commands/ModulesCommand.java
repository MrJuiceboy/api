package fr.hyneria.api.commands;

import fr.hyneria.api.database.caches.bungeecord.BungeeCache;
import fr.hyneria.api.database.caches.rabbitmq.RabbitCache;
import fr.hyneria.api.database.caches.redis.RedisCache;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.games.manager.server.SManager;
import fr.hyneria.api.utils.message.MessagesUtils;

public class ModulesCommand implements CommandExecutor {
	
	/**
	 * Liste des modules
	 */
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		if (cmd.getName().equals("modules")) {
			if (sender.isOp()) send(sender);
			else sender.sendMessage(MessagesUtils.getMessageNoPermission());
		}
		return false;
	}
	
	/**
	 * Envoyer au sender de la commande la liste des modules charg�s
	 * @param sender
	 */
	public void send(CommandSender sender) {
		sender.sendMessage("�e>>> �b[�eModules�b] �e<<<");
		sender.sendMessage("�bBungeeCache: �e" + BungeeCache.instance.isConnected());
		sender.sendMessage("�bRabbitCache: �e" + RabbitCache.instance.isConnected());
		sender.sendMessage("�bRedisCache: �e" + RedisCache.instance.isConnected());
		sender.sendMessage("�bGameServer: �e" + GameServer.getInstance().isConnected());
		sender.sendMessage("�bServerManager: �e" + SManager.getInstance().isConnected());
	}	
}
