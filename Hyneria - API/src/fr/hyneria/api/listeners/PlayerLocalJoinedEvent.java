package fr.hyneria.api.listeners;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.games.gameserver.GameServer;
import fr.hyneria.api.listeners.customs.callers.events.players.join.PlayerJoinedEvent;
import fr.hyneria.api.listeners.customs.callers.events.players.join.PlayersJoinedEvent;
import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerLocalJoinedEvent extends HyneriaListener {
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerLocalJoinedEvent(PlayerJoinEvent event) {
        if (GameServer.getInstance().isConnected()) return;
        // Le serveur o� le joueur est n'est pas reli� � un syst�me sp�ciale
        // du coup le PlayerJoinedEvent ne peut pas fired, donc on le fait � la main

        // On sort le Player
        Player player = event.getPlayer();

        // Cr�ation du StandPlayer
        HyneriaPlayer hyneriaPlayer = HyneriaPlayer.createPlayer(player);

        // Join !
        sendJoin(player, hyneriaPlayer);
    }

    public static void sendJoin(Player player, HyneriaPlayer hyneriaPlayer) {
        if (hyneriaPlayer.party != null && !hyneriaPlayer.party.isEmpty()) {
            // Contains player
            boolean already = false;
            for (HyneriaPlayer sPlayer : HyneriaPlayer.players.values()) {
                if (!hyneriaPlayer.equals(sPlayer)) {
                    if (sPlayer.party.contains(hyneriaPlayer.getUniqueId())) {
                        already = true;
                        break;
                    }
                }
            }
            if (already) return;
            List<Player> players = new ArrayList<>();
            List<HyneriaPlayer> standPlayers = new ArrayList<>();
            for (UUID uuid : hyneriaPlayer.party) {
                Player p = HyneriaAPI.getInstance().getServer().getPlayer(uuid);
                if (p == null || !p.isOnline()) continue;
                players.add(p);
                HyneriaPlayer sPlayer = HyneriaPlayer.getPlayer(p);
                standPlayers.add(sPlayer);
            }
            // On fire l'event
            HyneriaAPI.getInstance().getServer().getPluginManager().callEvent(new PlayersJoinedEvent(players, standPlayers));
            return;
        }
        // On fire l'event
        HyneriaAPI.getInstance().getServer().getPluginManager().callEvent(new PlayerJoinedEvent(player, hyneriaPlayer));
    }

}
