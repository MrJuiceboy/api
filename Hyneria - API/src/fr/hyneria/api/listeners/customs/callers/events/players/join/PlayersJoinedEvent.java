package fr.hyneria.api.listeners.customs.callers.events.players.join;

import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

/**
 * Cet event est appel� lors de la connexion d'une party de joueurs,
 * cela permet de r�cup�rer les standplayers
 * PlayersJoinedEvent est appel� une fois que tous les joueurs de la party sont connect�s
 * et que leurs infos (oplevel, etc...) ont �t� envoy�s.
 * L'utilisation du PlayerJoinEvent est maintenant d�pr�ci�e,
 * merci d'utiliser cet event.
 */
public class PlayersJoinedEvent extends Event {

    // FIELDS
    private static final HandlerList handlers	= new HandlerList();

    private List<Player> players;
    private long 		 	  time = 0L;
    private List<HyneriaPlayer> hyneriaPlayers;

    /**
     * Constructeur de connexion d'une party
     * @param players
     * @param standPlayers
     */
    public PlayersJoinedEvent(List<Player> players, List<HyneriaPlayer> hyneriaPlayers) {
        this.players = players;
        this.hyneriaPlayers = hyneriaPlayers;
        this.time = System.currentTimeMillis();
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public List<HyneriaPlayer> getStandPlayers() {
        return this.hyneriaPlayers;
    }

    public long getLoginTime() {
        return this.time;
    }

    /**
     * Cancel le join de tous les joueurs
     * @param reason
     */
    public void cancelJoin(String reason) {
        for (Player player : players)
            if (player.isOnline())
                player.kickPlayer(reason);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}

