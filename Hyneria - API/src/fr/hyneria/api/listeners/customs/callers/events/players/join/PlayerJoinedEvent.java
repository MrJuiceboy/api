package fr.hyneria.api.listeners.customs.callers.events.players.join;

import fr.hyneria.api.players.HyneriaPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Cet event est appel� lors de la connexion d'un joueur,
 * cela permet de r�cup�rer le standPlayer
 * PlayerJoinedEvent est appel� une fois que le joueur est connect�
 * et que ses infos (oplevel, etc...) ont �t� envoy�s.
 * L'utilisation du PlayerJoinEvent est maintenant d�pr�ci�e,
 * merci d'utiliser cet event.
 *
 * {@value CET EVENT N'EST PAS APPEL� QUAND LE JOUEUR EST DANS UNE PARTY, CA APPELLE PLAYERSJOINEDEVENT}
 *
 */
public class PlayerJoinedEvent extends PlayerEvent {

    // FIELDS
    private static final HandlerList handlers	= new HandlerList();

    private long 		time = 0L;
    private HyneriaPlayer hyneriaPlayer;

    public PlayerJoinedEvent(Player player, HyneriaPlayer hyneriaPlayer) {
        super(player);
        this.hyneriaPlayer = hyneriaPlayer;
        this.time = System.currentTimeMillis();
    }

    public HyneriaPlayer getHyneriaPlayer() {
        return this.hyneriaPlayer;
    }

    public long getLoginTime() {
        return this.time;
    }

    /**
     * Cancel le join d'un joueur
     * @param reason
     */
    public void cancelJoin(String reason) {
        player.kickPlayer(reason);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
