package fr.hyneria.api.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.hyneria.api.players.HyneriaPlayer;

/**
 * AntiSpam
 */
public class ChatSpamListener extends HyneriaListener {
	
	@EventHandler (priority = EventPriority.LOWEST)
	public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		HyneriaPlayer bbp = HyneriaPlayer.getPlayer(player);
		if (event.getMessage().length() <= 1) {
			event.setCancelled(true); 
			return;
		}
		if (bbp == null) return;
		long reste = time() - bbp.tempLastMessageTime;
		if (reste < 1000L) {
			event.setCancelled(true);
			player.sendMessage("�7Veuillez patienter entre chaque message.");
			bbp.tempLastMessageTime = time();
			return;
		}
		if (bbp.tempLastMessage != null && !bbp.tempLastMessage.equals(null)) {
			if (bbp.tempLastMessage.equalsIgnoreCase(event.getMessage())) {
				event.setCancelled(true);
				player.sendMessage("�7Vous avez d�j� envoy� un message presque similaire il y a peu de temps.");
			}
		}
		bbp.tempLastMessage = event.getMessage();
		bbp.tempLastMessageTime = time();
	}
	
}
