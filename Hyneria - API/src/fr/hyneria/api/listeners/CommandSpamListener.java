package fr.hyneria.api.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import fr.hyneria.api.players.HyneriaPlayer;

/**
 * AntiSpam
 */
public class CommandSpamListener extends HyneriaListener {

	@EventHandler (priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerCommandPreProcess(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		HyneriaPlayer bbp = HyneriaPlayer.getPlayer(player);
		if (bbp == null) return;
		long reste = time() - bbp.tempLastCommand;
		if (reste < 500L) {
			event.setCancelled(true);
			player.sendMessage("�7Veuillez patienter entre chaque commande.");
		}
		bbp.tempLastCommand = time();
	}
	
}
