package fr.hyneria.api.listeners;

import java.util.Arrays;
import java.util.List;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.log.LoggerUtils;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

/**
 * Don't show infos of commands (/help or plugins)
 */
public class AntiTabInfoListener extends HyneriaListener {

	/** FIELDS **/
	private final String[] badCommands   	= new String[] { "/ver", "/about", "/icanhasbukkit", "/help", "/?", "/bukkit", "/pl", "/plugins", "/me", "/minecraft", "/mojang", "/craftbukkit" };
	private ProtocolManager protocolManager = null;
	private String 			message			= "�cNope!";

	/**
	 * Constructor of the AntiTabInfoListener class
	 */
	public AntiTabInfoListener() {
		// Setting the packet manager
		this.protocolManager = ProtocolLibrary.getProtocolManager();
		// Load handles
		this.handleInTab(); // load in tabcomplete handling packet
		// As we do not know all the commands for viewing plugins from the tablist, we block the tabcomplete when the results are strange
		this.handleOutTab(); // load in tabcomplete handling packet
	}

	/**
	 * Getting the packet manager
	 * @return a PacketManager interface
	 */
	private ProtocolManager getPacketManager() {
		return this.protocolManager;
	}

	private void sendMessage(Player player) {
		if (this.message == null) return;
		if ("".equals(this.message)) return;
		player.sendMessage(this.message);
	}

	/**
	 * Blocking trying to access to the plugins with the tab complete using commands
	 */
	private void handleInTab() {
		this.getPacketManager().addPacketListener(new PacketAdapter(HyneriaAPI.getInstance(), ListenerPriority.NORMAL, PacketType.Play.Client.TAB_COMPLETE) {
			@Override
			public void onPacketReceiving(PacketEvent event) {
				PacketContainer packet = event.getPacket();
				String message = (String) packet.getSpecificModifier(String.class).read(0);

				if (!message.startsWith("/"))
					return;

				for (String command : badCommands) {
					if (message.startsWith(command)) {
						event.setCancelled(true);
						sendMessage(event.getPlayer());
						break;
					}
				}
			}
		});
	}

	/** Blocking trying to access to the plugins with the tab complete
	 *  This detect if the base plugin of the network is on the server and showed for cancelling the results
	 */
	private void handleOutTab() {
		this.getPacketManager().addPacketListener(new PacketAdapter(HyneriaAPI.getInstance(), ListenerPriority.NORMAL, PacketType.Play.Server.TAB_COMPLETE) {
			@Override
			public void onPacketSending(PacketEvent event) {
				PacketContainer packet = event.getPacket();
				String[] message = (String[]) packet.getStringArrays().read(0);

				List<String> messages = Arrays.asList(message);
				if (messages.contains(HyneriaAPI.getInstance().getServer().getServerName())) {
					event.setCancelled(true);
					sendMessage(event.getPlayer());
					return;
				}

				for (String command : badCommands) {
					if (messages.contains(command) || messages.contains("/")) {
						event.setCancelled(true);
						sendMessage(event.getPlayer());
						break;
					}
				}
			}
		});
	}

	/**
	 * Initialize the AntiTabInfoListener instance
	 */
	public static void init() {
		try {
			// Create a new instance

			AntiTabInfoListener.class.newInstance();
		}catch (Exception error) {
			// Error on creating a new instance of AntiTabInfoListener class
			LoggerUtils.warning("�cUnable to create a new instance for th AntiTabInfoListener class.");
			LoggerUtils.warning("�cError:");
			// Print
			error.printStackTrace();
		}
	}

}
