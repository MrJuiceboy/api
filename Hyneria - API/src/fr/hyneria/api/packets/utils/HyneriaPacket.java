package fr.hyneria.api.packets.utils;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.commons.lang3.concurrent.LazyInitializer;

import fr.hyneria.api.players.HyneriaPlayer;
import fr.hyneria.api.utils.common.FieldAccess;
import net.minecraft.server.v1_12_R1.Packet;

@SuppressWarnings("rawtypes")
public abstract class HyneriaPacket<T extends Packet> {

    private T 			handle;
    private HyneriaPlayer receiver;
    private boolean		cancel;

    /**
     * Constructor of the packet
     * @param handle
     * @param receiver
     */
    public HyneriaPacket(T handle, HyneriaPlayer receiver) {
        this.handle = handle;
        this.receiver = receiver;
    }

    /**
     * Getting the nms packet
     * @return
     */
    public T toNms() {
        return this.handle;
    }

    /**
     * Cancel l'envoi du packet ou non
     * @param cancel
     */
    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    /**
     * Savoir si le packet est cancel
     * @return
     */
    public boolean isCancelled() {
        return this.cancel;
    }

    // Field access
    private LazyInitializer<FieldAccess> access = new LazyInitializer<FieldAccess>() {
        @Override
        protected FieldAccess initialize() throws ConcurrentException {
            return new FieldAccess(handle);
        }
    };

    /**
     * Reloads the field accessor when the handle has changed
     */
    @SuppressWarnings("unused")
	private void reinitAccess() {
        access = null;
        access = new LazyInitializer<FieldAccess>() {
            @Override
            protected FieldAccess initialize() throws ConcurrentException {
                return new FieldAccess(handle);
            }
        };
    }

    /**
     * Packet field accessor.
     *
     * @return field accessor.
     */
    public FieldAccess access() {
        try {
            return access.get();
        } catch (ConcurrentException ignored) {
            return null;
        }
    }

    /**
     * Getting the receiver of the packet
     * @return
     */
    public HyneriaPlayer getReceiver() {
        return this.receiver;
    }

    /**
     * Getting the packet type
     * @return
     */
    public abstract PacketType getType();

}