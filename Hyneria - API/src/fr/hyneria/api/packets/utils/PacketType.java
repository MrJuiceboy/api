package fr.hyneria.api.packets.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInAbilities;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInArmAnimation;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInBlockDig;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInBlockPlace;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInChat;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInClientCommand;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInCloseWindow;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInCustomPayload;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInEnchantItem;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInEntityAction;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInFlying;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInHeldItemSlot;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInKeepAlive;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInLook;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInPosition;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInPositionLook;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInResourcePackStatus;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInSetCreativeSlot;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInSettings;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInSpectate;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInSteerVehicle;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInTabComplete;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInTransaction;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInUpdateSign;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInUseEntity;
import fr.hyneria.api.packets.list.play.in.HyneriaPacketPlayInWindowClick;

@SuppressWarnings("rawtypes")
public class PacketType<T extends HyneriaPacket> {

    public static class Client {
        public static class Play {
            public static class In {

				@SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInFlying> FLYING = new PacketType(HyneriaPacketPlayInFlying.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInAbilities> ABILITIES = new PacketType(HyneriaPacketPlayInAbilities.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInArmAnimation> ARM_ANIMATION = new PacketType(HyneriaPacketPlayInArmAnimation.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInBlockDig> BLOCK_DIG = new PacketType(HyneriaPacketPlayInBlockDig.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInBlockPlace> BLOCK_PLACE = new PacketType(HyneriaPacketPlayInBlockPlace.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInChat> CHAT = new PacketType(HyneriaPacketPlayInChat.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInClientCommand> CLIENT_COMMAND = new PacketType(HyneriaPacketPlayInClientCommand.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInCloseWindow> CLOSE_WINDOW = new PacketType(HyneriaPacketPlayInCloseWindow.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInCustomPayload> CUSTOM_PAYLOAD = new PacketType(HyneriaPacketPlayInCustomPayload.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInEnchantItem> ENCHANT_ITEM = new PacketType(HyneriaPacketPlayInEnchantItem.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInEntityAction> ENTITY_ACTION = new PacketType(HyneriaPacketPlayInEntityAction.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInHeldItemSlot> HELD_ITEM_SLOT = new PacketType(HyneriaPacketPlayInHeldItemSlot.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInKeepAlive> KEEP_ALIVE = new PacketType(HyneriaPacketPlayInKeepAlive.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInLook> LOOK = new PacketType(HyneriaPacketPlayInLook.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInPositionLook> POSITION_LOOK = new PacketType(HyneriaPacketPlayInPositionLook.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInPosition> POSITION = new PacketType(HyneriaPacketPlayInPosition.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInResourcePackStatus> RESOURCE_PACK_STATUS = new PacketType(HyneriaPacketPlayInResourcePackStatus.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInSetCreativeSlot> SET_CREATIVE_SLOT = new PacketType(HyneriaPacketPlayInSetCreativeSlot.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInSettings> SETTINGS = new PacketType(HyneriaPacketPlayInSettings.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInSpectate> SPECTATE = new PacketType(HyneriaPacketPlayInSpectate.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInSteerVehicle> STEER_VEHICLE = new PacketType(HyneriaPacketPlayInSteerVehicle.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInTabComplete> TAB_COMPLETE = new PacketType(HyneriaPacketPlayInTabComplete.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInTransaction> TRANSACTION = new PacketType(HyneriaPacketPlayInTransaction.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInUpdateSign> UPDATE_SIGN = new PacketType(HyneriaPacketPlayInUpdateSign.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInUseEntity> USE_ENTITY = new PacketType(HyneriaPacketPlayInUseEntity.class, Protocol.PLAY, Bound.IN);
                @SuppressWarnings("unchecked")
				public static final PacketType<HyneriaPacketPlayInWindowClick> WINDOW_CLICK = new PacketType(HyneriaPacketPlayInWindowClick.class, Protocol.PLAY, Bound.IN);
            }
        }
    }

    private Class<HyneriaPacket> 			hyneriaPacket;
    private Protocol 					 protocol;
    private Bound    					 bound;

    public PacketType(Class<HyneriaPacket> hyneriaPacket, Protocol protocol, Bound bound) {
        this.hyneriaPacket = hyneriaPacket;
        this.protocol    = protocol;
        this.bound       = bound;
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public Bound getBound() {
        return this.bound;
    }

    public Class<HyneriaPacket> getHyneriaPacket() {
        return this.hyneriaPacket;
    }


    public static void registerPackets(Map<Class<HyneriaPacket>, PacketType> packets, Map<Class<HyneriaPacket>, List<PacketListener>> listeners) {
        registerPacket(Client.Play.In.FLYING, packets, listeners);
        registerPacket(Client.Play.In.ABILITIES, packets, listeners);
        registerPacket(Client.Play.In.ARM_ANIMATION, packets, listeners);
        registerPacket(Client.Play.In.BLOCK_DIG, packets, listeners);
        registerPacket(Client.Play.In.BLOCK_PLACE, packets, listeners);
        registerPacket(Client.Play.In.CHAT, packets, listeners);
        registerPacket(Client.Play.In.CLIENT_COMMAND, packets, listeners);
        registerPacket(Client.Play.In.CLOSE_WINDOW, packets, listeners);
        registerPacket(Client.Play.In.CUSTOM_PAYLOAD, packets, listeners);
        registerPacket(Client.Play.In.ENCHANT_ITEM, packets, listeners);
        registerPacket(Client.Play.In.ENTITY_ACTION, packets, listeners);
        registerPacket(Client.Play.In.HELD_ITEM_SLOT, packets, listeners);
        registerPacket(Client.Play.In.KEEP_ALIVE, packets, listeners);
        registerPacket(Client.Play.In.LOOK, packets, listeners);
        registerPacket(Client.Play.In.POSITION_LOOK, packets, listeners);
        registerPacket(Client.Play.In.POSITION, packets, listeners);
        registerPacket(Client.Play.In.RESOURCE_PACK_STATUS, packets, listeners);
        registerPacket(Client.Play.In.SET_CREATIVE_SLOT, packets, listeners);
        registerPacket(Client.Play.In.SETTINGS, packets, listeners);
        registerPacket(Client.Play.In.SPECTATE, packets, listeners);
        registerPacket(Client.Play.In.STEER_VEHICLE, packets, listeners);
        registerPacket(Client.Play.In.TAB_COMPLETE, packets, listeners);
        registerPacket(Client.Play.In.TRANSACTION, packets, listeners);
        registerPacket(Client.Play.In.UPDATE_SIGN, packets, listeners);
        registerPacket(Client.Play.In.USE_ENTITY, packets, listeners);
        registerPacket(Client.Play.In.WINDOW_CLICK, packets, listeners);
    }

    @SuppressWarnings("unchecked")
	private static void registerPacket(PacketType packetType, Map<Class<HyneriaPacket>, PacketType> packets, Map<Class<HyneriaPacket>, List<PacketListener>> listeners) {
        packets.put(packetType.getHyneriaPacket(), packetType);
        listeners.put(packetType.getHyneriaPacket(), new ArrayList<PacketListener>());
    }

    public static enum Protocol {
        PLAY;
    }

    public static enum Bound {
        IN,
        OUT;
    }

}
