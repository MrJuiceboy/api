package fr.hyneria.api.packets;

import fr.hyneria.api.HyneriaAPI;
import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketListener;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class PacketManager {

    @SuppressWarnings("rawtypes")
	private Map<Class<HyneriaPacket>, PacketType> packets;
    @SuppressWarnings("rawtypes")
	private Map<Class<HyneriaPacket>, List<PacketListener>> listeners;

    public PacketManager() {
        packets   = new HashMap<>();
        listeners = new HashMap<>();
        PacketType.registerPackets(packets, listeners);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void registerListener(PacketListener packetListener) {
        if (!this.listeners.containsKey(packetListener.getType().getHyneriaPacket())) {
            System.out.println("Unknown " + packetListener.getType().getHyneriaPacket().getSimpleName() + " class");
            return;
        }
        Class<? extends HyneriaPacket> hyneriaPacket = packetListener.getType().getHyneriaPacket();
        List<PacketListener> l = this.listeners.get(hyneriaPacket);
        l.add(packetListener);
    }

    @SuppressWarnings("rawtypes")
	public void	sendDirectPacket(final HyneriaPlayer player, Packet packet){
        final Player p = Bukkit.getPlayer(player.getUniqueId());
        if (!HyneriaPlayer.players.containsKey(p.getUniqueId())) return;
        final EntityPlayer ent = ((CraftPlayer)p).getHandle();
        final Channel channel = ent.playerConnection.networkManager.channel;
        channel.pipeline().context("packet_handler").write(packet);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void unregisterListener(PacketListener packetListener) {
        if (!this.listeners.containsKey(packetListener.getType().getHyneriaPacket())) {
            System.out.println("Unknown " + packetListener.getType().getHyneriaPacket().getSimpleName() + " class");
            return;
        }
        Class<? extends HyneriaPacket> hyneriaPacket = packetListener.getType().getHyneriaPacket();
        List<PacketListener> l = this.listeners.get(hyneriaPacket);
        l.remove(packetListener);
    }

    public void loadPlayer(final HyneriaPlayer player) {
        Bukkit.getScheduler().runTaskAsynchronously(HyneriaAPI.getInstance(), new Runnable() {
            public void run() {
                final Player p = Bukkit.getPlayer(player.getUniqueId());
                final EntityPlayer ent = ((CraftPlayer)p).getHandle();
                final Channel channel = ent.playerConnection.networkManager.channel;
                channel.pipeline().addBefore("packet_handler", "hyneriaapi", new ChannelDuplexHandler(){
                    @SuppressWarnings("rawtypes")
					@Override
                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                        if (!p.isOnline()) return;
                        String message = msg.getClass().getName().substring(29);
                        boolean cancel = false;
                        String[] splitter = message.split("Packet");
                        for (Entry<Class<HyneriaPacket>, PacketType> packet : packets.entrySet()) {
                            Class<HyneriaPacket> key = packet.getKey();
                            String name = key.getSimpleName().replace("HyneriaPacket", "");
                            if (splitter[1].equals(name) || (splitter.length > 2 && splitter[2].equals(name))) {
                                try {
                                    Class[] cArg = new Class[2];
                                    cArg[0] = Packet.class;
                                    cArg[1] = HyneriaPlayer.class;
                                    Packet pack = (Packet) msg;
                                    Constructor constructor = key.getConstructor(Packet.class, HyneriaPlayer.class);
                                    HyneriaPacket standPacket = (HyneriaPacket) constructor.newInstance(pack, player);
                                    for (PacketListener packetListener : listeners.get(key)) {
                                        // receive
                                        if (packetListener.getType().getBound().equals(PacketType.Bound.IN))
                                            packetListener.onReceivingPacket(standPacket);
                                        else if (packetListener.getType().getBound().equals(PacketType.Bound.OUT))
                                            packetListener.onSendingPacket(standPacket);
                                        if (standPacket.isCancelled()) cancel = true;
                                    }
                                }catch(Exception error) {
                                    error.printStackTrace();
                                }
                            }
                        }
                        if (!cancel)
                            super.channelRead(ctx, msg);
                    }
                });
            }
        });
    }
}
