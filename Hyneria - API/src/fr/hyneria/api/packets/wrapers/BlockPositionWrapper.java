package fr.hyneria.api.packets.wrapers;

import org.bukkit.block.Block;

import fr.hyneria.api.utils.common.FieldAccess;
import net.minecraft.server.v1_12_R1.BlockPosition;

public class BlockPositionWrapper  {

    private final BlockPosition position;
    private FieldAccess access;

    public BlockPositionWrapper(BlockPosition position) {
        this.position = position;
        access = new FieldAccess(position);
    }

    public BlockPositionWrapper(Block block) {
        this(block.getX(), block.getY(), block.getZ());
    }

    public BlockPositionWrapper(int x, int y, int z) {
        this.position = new BlockPosition(x, y, z);
        access = new FieldAccess(position);
    }

    public int getX() {
        return position.getX();
    }

    public int getY() {
        return position.getY();
    }

    public int getZ() {
        return position.getZ();
    }

    public void setX(int x) {
        access.set("a", x);
    }

    public void setY(int x) {
        access.set("c", x);
    }

    public void setZ(int x) {
        access.set("d", x);
    }

    public BlockPosition getNMS() {
        return position;
    }

}