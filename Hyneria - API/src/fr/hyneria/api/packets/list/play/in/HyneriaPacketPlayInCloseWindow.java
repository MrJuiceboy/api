package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInCloseWindow;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInCloseWindow<T extends PacketPlayInCloseWindow> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInCloseWindow(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.CLOSE_WINDOW;
    }

    public int getWindowId() {
        return (int) access().get("id");
    }

}
