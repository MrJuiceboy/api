package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInHeldItemSlot;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInHeldItemSlot<T extends PacketPlayInHeldItemSlot> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInHeldItemSlot(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.HELD_ITEM_SLOT;
    }

    public int getNewHeldSlot() {
        return (int) access().get("itemInHandIndex");
    }

    public HyneriaPacketPlayInHeldItemSlot setNewHeldSlot(int itemInHandIndex) {
        access().set("itemInHandIndex", itemInHandIndex);
        return this;
    }

}
