package fr.hyneria.api.packets.list.play.in;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInUpdateSign;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInUpdateSign<T extends PacketPlayInUpdateSign> extends HyneriaPacket {

    private final Gson gson;

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInUpdateSign(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(BaseComponent.class, new ComponentSerializer());
        builder.registerTypeAdapter(IChatBaseComponent.class, new IChatBaseComponent.ChatSerializer());
        gson = builder.create();
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.UPDATE_SIGN;
    }

    private BaseComponent cast(IChatBaseComponent component) {
        String json = gson.toJson(component);
        return gson.fromJson(json, BaseComponent.class);
    }

    public BaseComponent[] getLines() {
        IChatBaseComponent[] packet = (IChatBaseComponent[]) access().get("b");
        BaseComponent[] ret = new BaseComponent[packet.length];
        for (int i = 0; i < packet.length; i++) {
            ret[i] = cast(packet[i]);
        }
        return ret;
    }

}
