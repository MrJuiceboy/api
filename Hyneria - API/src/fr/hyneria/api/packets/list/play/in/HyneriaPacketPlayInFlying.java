package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInFlying;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInFlying<T extends PacketPlayInFlying> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInFlying(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.FLYING;
    }

    public boolean isOnGround() {
        return (boolean) access().get("f");
    }

}
