package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.packets.wrapers.BlockPositionWrapper;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.EnumDirection;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInBlockDig;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInBlockDig<T extends PacketPlayInBlockDig> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInBlockDig(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.BLOCK_DIG;
    }

    public BlockPositionWrapper getPosition() {
        return new BlockPositionWrapper((BlockPosition) access().get("a"));
    }

    public EnumDirection getDirection() {
        return (EnumDirection) access().get("b");
    }

    public PacketPlayInBlockDig.EnumPlayerDigType getDigType() {
        return (PacketPlayInBlockDig.EnumPlayerDigType) access().get("c");
    }

    public HyneriaPacketPlayInBlockDig setPosition(BlockPositionWrapper val) {
        access().set("a", val.getNMS());
        return this;
    }

    public HyneriaPacketPlayInBlockDig setDirection(EnumDirection val) {
        access().set("b", val);
        return this;
    }

    public HyneriaPacketPlayInBlockDig setDigType(PacketPlayInBlockDig.EnumPlayerDigType val) {
        access().set("c", val);
        return this;
    }

}

