package fr.hyneria.api.packets.list.play.in;

import org.bukkit.util.Vector;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInUseEntity;
import net.minecraft.server.v1_12_R1.Vec3D;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInUseEntity<T extends PacketPlayInUseEntity> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInUseEntity(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.USE_ENTITY;
    }

    public Vector getBlockInteractAt() {
        Vec3D vec = (Vec3D) access().get("c");
        if (vec == null)
            return null;
        return new Vector(vec.x, vec.y, vec.z);
    }

}
