package fr.hyneria.api.packets.list.play.in;

import java.util.HashMap;
import java.util.Map;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInEntityAction;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInEntityAction<T extends PacketPlayInEntityAction> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInEntityAction(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.ENTITY_ACTION;
    }

    public Action getAction() {
        return Action.fromNMS((PacketPlayInEntityAction.EnumPlayerAction) access().get("animation"));
    }

    public int getRidingJumpBoost() {
        return (int) access().get("c");
    }

    public int getEntityId() {
        return (int) access().get("a");
    }

    public HyneriaPacketPlayInEntityAction setAction(Action action) {
        access().set("animation", action.nms);
        return this;
    }

    public HyneriaPacketPlayInEntityAction setRidingJumpBoost(int jumpBoost) {
        access().set("c", jumpBoost);
        return this;
    }

    public HyneriaPacketPlayInEntityAction setEntityId(int eid) {
        access().set("a", eid);
        return this;
    }

    public enum Action {
        START_SNEAKING(PacketPlayInEntityAction.EnumPlayerAction.START_SNEAKING),
        STOP_SNEAKING(PacketPlayInEntityAction.EnumPlayerAction.STOP_SNEAKING),
        STOP_SLEEPING(PacketPlayInEntityAction.EnumPlayerAction.STOP_SLEEPING),
        START_SPRINTING(PacketPlayInEntityAction.EnumPlayerAction.START_SPRINTING),
        STOP_SPRINTING(PacketPlayInEntityAction.EnumPlayerAction.STOP_SPRINTING),
        OPEN_INVENTORY(PacketPlayInEntityAction.EnumPlayerAction.OPEN_INVENTORY);

        private static Map<PacketPlayInEntityAction.EnumPlayerAction, Action> commands = new HashMap<>();

        private PacketPlayInEntityAction.EnumPlayerAction nms;

        Action(PacketPlayInEntityAction.EnumPlayerAction nms) {
            this.nms = nms;
        }

        static {
            for (Action command : values())
                commands.put(command.nms, command);
        }

        protected static Action fromNMS(PacketPlayInEntityAction.EnumPlayerAction nms) {
            return commands.get(nms);
        }

        protected PacketPlayInEntityAction.EnumPlayerAction toNms() {
            return nms;
        }
    }

}

