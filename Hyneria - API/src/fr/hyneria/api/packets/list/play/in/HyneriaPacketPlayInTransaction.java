package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInTransaction;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInTransaction<T extends PacketPlayInTransaction> extends HyneriaPacket {

    @SuppressWarnings({ "unchecked" })
	public HyneriaPacketPlayInTransaction(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.TRANSACTION;
    }

    public HyneriaPacketPlayInTransaction setWindowId(int id) {
        access().set("a", id);
        return this;
    }

    public int getWindowId() {
        return (int) access().get("a");
    }

    public HyneriaPacketPlayInTransaction setTransactionId(short id) {
        access().set("b", id);
        return this;
    }

    public short getTransactionId() {
        return (short) access().get("b");
    }

    public HyneriaPacketPlayInTransaction setAccepted(boolean accepted) {
        access().set("c", accepted);
        return this;
    }

    public boolean isAccepted() {
        return (boolean) access().get("c");
    }

}
