package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInKeepAlive;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInKeepAlive<T extends PacketPlayInKeepAlive> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInKeepAlive(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.KEEP_ALIVE;
    }

    public int getKeepAliveId() {
        return (int) access().get("a");
    }

}
