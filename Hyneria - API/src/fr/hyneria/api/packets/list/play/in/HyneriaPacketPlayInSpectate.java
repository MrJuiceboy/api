package fr.hyneria.api.packets.list.play.in;

import java.util.UUID;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInSpectate;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInSpectate<T extends PacketPlayInSpectate> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInSpectate(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.SPECTATE;
    }

    public UUID getTargetPlayer() {
        return (UUID) access().get("a");
    }

}
