package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInFlying.PacketPlayInPositionLook;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInPositionLook<T extends PacketPlayInPositionLook> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInPositionLook(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.POSITION_LOOK;
    }

    public float getYaw() {
        return (float) access().get("yaw");
    }

    public float getPitch() {
        return (float) access().get("pitch");
    }

    public double getX() {
        return (double) access().get("x");
    }

    public double getY() {
        return (double) access().get("y");
    }

    public double getZ() {
        return (double) access().get("z");
    }

    public boolean isOnGround() {
        return (boolean) access().get("f");
    }
}

