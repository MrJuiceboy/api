package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInSteerVehicle;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInSteerVehicle<T extends PacketPlayInSteerVehicle> extends HyneriaPacket {

    @SuppressWarnings({ "unchecked" })
	public HyneriaPacketPlayInSteerVehicle(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.STEER_VEHICLE;
    }

    /**
     * Gets the sideways of the vehicle
     * Left of the player is positive
     * @return The sideways
     */
    public float getSideways() {
        return (float) access().get("a");
    }

    /**
     * Gets the forward of the vehicle
     * Forward is positive
     * @return The forward
     */
    public float getForward() {
        return (float) access().get("b");
    }

    public boolean isJumping() {
        return (boolean) access().get("c");
    }

    public boolean isUnmounting() {
        return (boolean) access().get("d");
    }

    // Setters
    public HyneriaPacketPlayInSteerVehicle setSideways(float value) {
        access().set("a", value);
        return this;
    }

    public HyneriaPacketPlayInSteerVehicle setForward(float value) {
        access().set("b", value);
        return this;
    }

    public HyneriaPacketPlayInSteerVehicle setJumping(boolean value) {
        access().set("c", value);
        return this;
    }

    public HyneriaPacketPlayInSteerVehicle setUnmounting(boolean value) {
        access().set("d", value);
        return this;
    }

}
