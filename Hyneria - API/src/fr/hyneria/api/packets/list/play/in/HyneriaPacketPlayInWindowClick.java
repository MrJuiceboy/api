package fr.hyneria.api.packets.list.play.in;

import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInWindowClick;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInWindowClick<T extends PacketPlayInWindowClick> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInWindowClick(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.WINDOW_CLICK;
    }

    public ItemStack getItem() {
        return (ItemStack) access().get("item");
    }

    public org.bukkit.inventory.ItemStack getBukkitItem() {
        return CraftItemStack.asBukkitCopy(getItem());
    }

    public short getTransactionId() {
        return (short) access().get("d");
    }

    public int getWindowId() {
        return (int) access().get("a");
    }

    public HyneriaPacketPlayInWindowClick setItem(org.bukkit.inventory.ItemStack item) {
        access().set("item", CraftItemStack.asNMSCopy(item));
        return this;
    }

    public HyneriaPacketPlayInWindowClick setTransactionId(short transactionId) {
        access().set("d", transactionId);
        return this;
    }

    public HyneriaPacketPlayInWindowClick setWindowId(int windowId) {
        access().set("a", windowId);
        return this;
    }

    public HyneriaPacketPlayInWindowClick setSlot(int slot) {
        access().set("slot", slot);
        return this;
    }

    public HyneriaPacketPlayInWindowClick setButton(int button) {
        access().set("button", button);
        return this;
    }

    public HyneriaPacketPlayInWindowClick setMode(int mode) {
        access().set("shift", mode);
        return this;
    }

}
