package fr.hyneria.api.packets.list.play.in;

import java.util.HashMap;
import java.util.Map;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInClientCommand;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInClientCommand<T extends PacketPlayInClientCommand> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInClientCommand(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.CLIENT_COMMAND;
    }

    public Command getCommand() {
        return Command.fromNMS((PacketPlayInClientCommand.EnumClientCommand) access().get("a"));
    }

    public HyneriaPacketPlayInClientCommand setCommand(Command command) {
        access().set("a", command.toNms());
        return this;
    }

    public enum Command {
        RESPAWN(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN),
        REQUEST_STATS(PacketPlayInClientCommand.EnumClientCommand.REQUEST_STATS);

        private static Map<PacketPlayInClientCommand.EnumClientCommand, Command> commands = new HashMap<>();

        private PacketPlayInClientCommand.EnumClientCommand nms;
        Command(PacketPlayInClientCommand.EnumClientCommand nms) {
            this.nms = nms;
        }

        static {
            for (Command command : values())
                commands.put(command.nms, command);
        }

        protected static Command fromNMS(PacketPlayInClientCommand.EnumClientCommand nms) {
            return commands.get(nms);
        }

        protected PacketPlayInClientCommand.EnumClientCommand toNms() {
            return nms;
        }
    }
}