package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInChat;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInChat<T extends PacketPlayInChat> extends HyneriaPacket {

    @SuppressWarnings({ "unchecked" })
	public HyneriaPacketPlayInChat(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.CHAT;
    }

    public String getMessage() {
        return (String) access().get("a");
    }

    public HyneriaPacketPlayInChat setMessage(String message) {
        access().set("a", message);
        return this;
    }
}
