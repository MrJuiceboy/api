package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketDataSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayInCustomPayload;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInCustomPayload<T extends PacketPlayInCustomPayload> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInCustomPayload(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.CUSTOM_PAYLOAD;
    }

    public String getChannel() {
        return (String) access().get("a");
    }

    public PacketDataSerializer getData() {
        return (PacketDataSerializer) access().get("b");
    }

}
