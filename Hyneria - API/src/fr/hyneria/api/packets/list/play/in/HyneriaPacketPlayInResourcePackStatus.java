package fr.hyneria.api.packets.list.play.in;

import java.util.HashMap;
import java.util.Map;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInResourcePackStatus;
import net.minecraft.server.v1_12_R1.PacketPlayInResourcePackStatus.EnumResourcePackStatus;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInResourcePackStatus<T extends PacketPlayInResourcePackStatus> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInResourcePackStatus(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.RESOURCE_PACK_STATUS;
    }

    @Deprecated
    public EnumResourcePackStatus getDownloadStatus() {
        return (EnumResourcePackStatus) access().get("b");
    }

    public enum Status {
        SUCCESSFULLY_LOADED(EnumResourcePackStatus.SUCCESSFULLY_LOADED),
        DECLINED(EnumResourcePackStatus.DECLINED),
        FAILED_DOWNLOAD(EnumResourcePackStatus.FAILED_DOWNLOAD),
        ACCEPTED(EnumResourcePackStatus.ACCEPTED);

        private static Map<PacketPlayInResourcePackStatus.EnumResourcePackStatus, Status> commands = new HashMap<>();

        private PacketPlayInResourcePackStatus.EnumResourcePackStatus nms;

        Status(PacketPlayInResourcePackStatus.EnumResourcePackStatus nms) {
            this.nms = nms;
        }

        static {
            for (Status command : values())
                commands.put(command.nms, command);
        }

        protected static Status fromNMS(PacketPlayInResourcePackStatus.EnumResourcePackStatus nms) {
            return commands.get(nms);
        }

        protected PacketPlayInResourcePackStatus.EnumResourcePackStatus toNms() {
            return nms;
        }
    }

}
