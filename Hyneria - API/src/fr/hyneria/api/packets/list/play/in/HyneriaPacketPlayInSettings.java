package fr.hyneria.api.packets.list.play.in;

import java.util.HashMap;
import java.util.Map;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInSettings;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInSettings<T extends PacketPlayInSettings> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInSettings(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.SETTINGS;
    }

    public String getLocale() {
        return (String) access().get("a");
    }

    public int getViewDistance() {
        return (int) access().get("b");
    }

    public ChatVisibility getChatVisibility() {
        return ChatVisibility.fromNMS((EntityHuman.EnumChatVisibility) access().get("c"));
    }

    public boolean areColorsEnabled() {
        return (boolean) access().get("d");
    }

    public enum ChatVisibility {
        FULL(EntityHuman.EnumChatVisibility.FULL),
        SYSTEM(EntityHuman.EnumChatVisibility.SYSTEM),
        HIDDEN(EntityHuman.EnumChatVisibility.HIDDEN);

        private static Map<EntityHuman.EnumChatVisibility, ChatVisibility> commands = new HashMap<>();

        private EntityHuman.EnumChatVisibility nms;

        ChatVisibility(EntityHuman.EnumChatVisibility nms) {
            this.nms = nms;
        }

        static {
            for (ChatVisibility command : values())
                commands.put(command.nms, command);
        }

        protected static ChatVisibility fromNMS(EntityHuman.EnumChatVisibility nms) {
            return commands.get(nms);
        }

        protected EntityHuman.EnumChatVisibility toNms() {
            return nms;
        }

    }

}
