package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInEnchantItem;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInEnchantItem<T extends PacketPlayInEnchantItem> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInEnchantItem(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.ENCHANT_ITEM;
    }

    public int getEnchantmentWindowId() {
        return (int) access().get("a");
    }

    public int getEnchantmentNumber() {
        return (int) access().get("b");
    }

    public HyneriaPacketPlayInEnchantItem setEnchantmentNumber(int enchantmentNumber) {
        access().set("b", enchantmentNumber);
        return this;
    }

}
