package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInFlying.PacketPlayInLook;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInLook<T extends PacketPlayInLook> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInLook(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.LOOK;
    }

    public float getYaw() {
        return (float) access().get("yaw");
    }

    public float getPitch() {
        return (float) access().get("pitch");
    }

    public boolean isOnGround() {
        return (boolean) access().get("f");
    }

}
