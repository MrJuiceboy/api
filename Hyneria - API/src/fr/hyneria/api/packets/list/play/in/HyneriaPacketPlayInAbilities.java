package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInAbilities;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInAbilities<T extends PacketPlayInAbilities> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInAbilities(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.ABILITIES;
    }

    public HyneriaPacketPlayInAbilities setAbility(Abilities ability, boolean value) {
        access().set(ability.fieldName, value);
        return this;
    }

    public boolean getAbility(Abilities ability) {
        return (boolean) access().get(ability.fieldName);
    }

    public HyneriaPacketPlayInAbilities setWalkSpeed(float speed) {
        access().set("f", speed);
        return this;
    }

    public HyneriaPacketPlayInAbilities setFlySpeed(float speed) {
        access().set("e", speed);
        return this;
    }

    public float getWalkSpeed() {
        return (float) access().get("f");
    }

    public float getFlySpeed() {
        return (float) access().get("e");
    }

    public enum Abilities {
        INVULNERABLE("a"),
        FLYING("b"),
        CANFLY("c"),
        CANINSTANTBUILD("d");

        private final String fieldName;

        Abilities(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getFieldName() {
            return fieldName;
        }
    }

}

