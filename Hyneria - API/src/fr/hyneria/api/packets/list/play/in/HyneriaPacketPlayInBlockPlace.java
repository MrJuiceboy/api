package fr.hyneria.api.packets.list.play.in;

import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.packets.wrapers.BlockPositionWrapper;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInBlockPlace;
import net.minecraft.server.v1_12_R1.Vector3f;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInBlockPlace<T extends PacketPlayInBlockPlace> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInBlockPlace(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.BLOCK_PLACE;
    }

    /**
     * Gets the position of the placed block
     * @return The position of the block or null if this packet represents a USE ITEM.
     */
    public BlockPositionWrapper getBlockPosition() {
        if (isInteract())
            return null;
        return new BlockPositionWrapper(getBlockPos());
    }

    private BlockPosition getBlockPos() {
        return (BlockPosition) access().get("a");
    }

    /**
     * Checks if this packet is a USE ITEM packet or a BLOCK PLACEMENT
     * @return true if this packet is a USE ITEM
     */
    public boolean isInteract() {
        BlockPosition pos = getBlockPos();
        return pos.getX() == -1 && pos.getY() == -1 && pos.getZ() == -1;
    }

    /**
     * Gets the item held by the player
     * @return The current item held by the player
     * @deprecated This field is ignored by the server
     */
    @Deprecated
    public ItemStack getHeldItem() {
        return CraftItemStack.asBukkitCopy((net.minecraft.server.v1_12_R1.ItemStack) access().get("d"));
    }

    /**
     * Gets the position of the cursor on the block
     * @return the position of the cursor on the block
     */
    public Vector3f getCursorPosition() {
        return new Vector3f(
                (float) access().get("e"),
                (float) access().get("f"),
                (float) access().get("g")
        );
    }

    /**
     * Gets the position of the placed block
     * @return The position of the block or null if this packet represents a USE ITEM.
     */
    public HyneriaPacketPlayInBlockPlace setBlockPosition(BlockPositionWrapper blockPosition) {
        access().set("a", blockPosition.getNMS());
        return this;
    }

    /**
     * Gets the item held by the player
     * @return The current item held by the player
     * @deprecated This field is ignored by the server
     */
    @Deprecated
    public HyneriaPacketPlayInBlockPlace setHeldItem(ItemStack item) {
        access().set("d", CraftItemStack.asNMSCopy(item));
        return this;
    }

    /**
     * Gets the position of the cursor on the block
     * @return the position of the cursor on the block
     */
    public HyneriaPacketPlayInBlockPlace setCursorPosition(Vector3f position) {
        access().set("e", position.getX());
        access().set("f", position.getY());
        access().set("g", position.getZ());
        return this;
    }
}

