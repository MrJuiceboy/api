package fr.hyneria.api.packets.list.play.in;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;
import fr.hyneria.api.players.HyneriaPlayer;
import net.minecraft.server.v1_12_R1.Packet;
import net.minecraft.server.v1_12_R1.PacketPlayInSetCreativeSlot;

@SuppressWarnings("rawtypes")
public class HyneriaPacketPlayInSetCreativeSlot<T extends PacketPlayInSetCreativeSlot> extends HyneriaPacket {

    @SuppressWarnings("unchecked")
	public HyneriaPacketPlayInSetCreativeSlot(Packet handle, HyneriaPlayer receiver) {
        super(handle, receiver);
    }

    @Override
    public PacketType getType() {
        return PacketType.Client.Play.In.SET_CREATIVE_SLOT;
    }

    public int getKeepAliveId() {
        return (int) access().get("a");
    }

}
