package fr.hyneria.api.packets;

import fr.hyneria.api.packets.utils.HyneriaPacket;
import fr.hyneria.api.packets.utils.PacketType;

public abstract class PacketListener<T extends PacketType> {

    private PacketType packetType;

    public PacketListener (PacketType packetType) {
        this.packetType = packetType;
    }

    public void onReceivingPacket(HyneriaPacket standPacket) {
        if (!standPacket.getType().getBound().equals(PacketType.Bound.IN)) {
            throw new RuntimeException("Please override onSendingPacket!");
        }
    }

    public void onSendingPacket(HyneriaPacket standPacket) {
        if (standPacket.getType().getBound().equals(PacketType.Bound.IN)) {
            throw new RuntimeException("Please override onReceivingPacket!");
        }
    }

    public PacketType getType() {
        return this.packetType;
    }

}
