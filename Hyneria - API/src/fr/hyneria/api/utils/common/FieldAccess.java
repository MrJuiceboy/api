package fr.hyneria.api.utils.common;

import com.google.common.collect.Maps;
import fr.hyneria.api.log.LoggerUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Helper for a fast reflection.
 */
public final class FieldAccess {

    // Object reflected
    private Object obj;
    private Class<?> clazz;

    // Field fast access
    private Map<String, Field> fields = Maps.newHashMap();

    /**
     * Create a new field accessor.
     *
     * @param obj object to reflect.
     */
    public FieldAccess(Object obj) {
        this(obj, obj.getClass());
    }

    /**
     * Create a new field accessor.
     *
     * @param obj   object to reflect.
     * @param clazz class to reflect.
     */
    public FieldAccess(Object obj, Class<?> clazz) {
        checkNotNull(obj, "The object to reflect can't be null");
        checkNotNull(clazz, "The class to reflect can't be null");
        this.obj = obj;
        this.clazz = clazz;
    }

    /**
     * Gets the value associated to a given non-static field.
     *
     * @param fieldName field to extract.
     * @return object or null if something is wrong.
     */
    public Object get(String fieldName) {
        Field field = extractField(fieldName);
        try {
            return FieldUtils.readField(field, obj, true);
        } catch (IllegalAccessException ex) {
            LoggerUtils.warning(ex.getMessage());
            return null;
        }
    }

    /**
     * Sets the value associated to a given non-static field.
     *
     * @param fieldName field to set.
     * @param value     new value of the field.
     */
    public void set(String fieldName, Object value) {
        Field field = extractField(fieldName);
        try {
            FieldUtils.writeField(field, obj, value, true);
        } catch (IllegalAccessException ex) {
            LoggerUtils.warning(ex.getMessage());
        }
    }

    /**
     * Extract the field.
     *
     * @param fieldName name of the field.
     * @return extract field.
     */
    private Field extractField(String fieldName) {
        Field field = fields.get(fieldName);
        if (field == null) {
            field = FieldUtils.getField(clazz, fieldName, true);
            fields.put(fieldName, field);
        }
        return field;
    }

    /**
     * Gets the value associated to a given static field.
     *
     * @param obj   object association.
     * @param field field to extract.
     * @return object or null if something is wrong.
     */
    @SuppressWarnings("rawtypes")
	public static Object getStatic(Class obj, String field) {
        try {
            return FieldUtils.readStaticField(obj, field, true);
        } catch (IllegalAccessException ex) {
            LoggerUtils.warning(ex.getMessage());
            return null;
        }
    }

    /**
     * Sets the value associated to a given static field.
     *
     * @param obj   object association.
     * @param field field to set.
     * @param value new value of the field.
     */
    @SuppressWarnings("rawtypes")
	public static void setStatic(Class obj, String field, Object value) {
        try {
            FieldUtils.writeStaticField(obj, field, value, true);
        } catch (IllegalAccessException ex) {
            LoggerUtils.warning(ex.getMessage());
        }
    }

}
