package fr.hyneria.api.utils.common;


public class BasicFunctionsUtil {

	// Instance Singleton
	private static BasicFunctionsUtil instance;

	public BasicFunctionsUtil() {
		instance = this;
	}

	// get
	public static BasicFunctionsUtil getInstance() {
		if (instance == null)
			new BasicFunctionsUtil();
		return instance;
	}

	public long time() {
		return System.currentTimeMillis();
	}
}
