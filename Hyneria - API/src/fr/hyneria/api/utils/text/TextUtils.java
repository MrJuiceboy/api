package fr.hyneria.api.utils.text;

public class TextUtils {

    private static TextUtils instance;

    /**
     * Constructor of the TextUtils instance
     */
    public TextUtils() {
        instance = this; // set de l'instance
    }

    /**
     * Getting the TextUtils instance. (singleton)
     * @return
     */
    public static TextUtils getInstance() {
        if (instance == null) new TextUtils();
        return instance;
    }

    /**
     * Get the plurial by a value ('s')
     * @param value
     * @return if value > 1 ['s'] else ['']
     */
    public String getPlural(double value) {
        return value > 1 ? "s" : "";
    }

    /**
     * Adding the plurial by a value ('s') to a word
     * @param value
     * @return if value > 1 ['s'] else ['']
     */
    public String addPlural(String word, double value) {
        return value > 1 ? word + "s" : word;
    }
}

