package fr.hyneria.api.utils.message;

import net.md_5.bungee.api.ChatColor;

public class MessagesUtils 
{
	static String prefix =  ChatColor.GRAY + "[" + ChatColor.GOLD + ChatColor.BOLD + "Hyneria" + ChatColor.GRAY + "] ";
	static String GovernorPrefix =  "�f�l[�cGOVERNOR�f] �e";
	static String Join = "�a a rejoint Hyneria.fr !";
	static String Achat_Successful = "�aAchat r�ussi !";
	static String Achat_Refusedful = "�cVous ne poss�dez pas assez de points.";
	static String Achat_Cancelled = "�cVous avez annul� l'achat.";
	static String Achat_Missing = "�cVous n'avez pas d'item correspondant � cette offre.";
	static String Achat_Already = "�cErreur, vous poss�dez d�j� cette article.";
	static String Achat_Rank = "�c�lPour une mise � jours compl�te, veuillez vous d�connecter et vous reconnecter.";
	static String no_permission = "�cVous n'avez pas la permission d'ex�cuter cette commande.";
	static String Game_Finish_Join_Lobby = "�7Vous rejoindrez le lobby dans quelques secondes.";
	static String Game_Only_Spectator_Full = "�cLa partie est pleine en sp�ctacteur !";
	static String Game_Join_Team_RED = "�7Tu as rejoint l'�quipe " + ChatColor.RED + "Rouge";
	static String Game_Join_Team_BLUE = "�7Tu as rejoint l'�quipe " + ChatColor.BLUE + "Bleu";
	static String Game_Join_Team_YELLOW = "�7Tu as rejoint l'�quipe " + ChatColor.YELLOW + "Jaune";
	static String Game_Join_Team_GREEN = "�7Tu as rejoint l'�quipe " + ChatColor.GREEN + "Vert";
	static String Game_Join_Team_RED_Full = "L'�quipe " + ChatColor.RED + "Rouge" + ChatColor.GRAY + " est d�j� pleine !";
	static String Game_Join_Team_BLUE_Full = "L'�quipe " + ChatColor.BLUE + "Bleu" + ChatColor.GRAY + " est d�j� pleine !";
	static String Game_Join_Team_YELLOW_Full = "L'�quipe " + ChatColor.YELLOW + "Jaune" + ChatColor.GRAY + " est d�j� pleine !";
	static String Game_Join_Team_GREEN_Full = "L'�quipe " + ChatColor.GREEN + "Vert" + ChatColor.GRAY + " est d�j� pleine !";
	static String Game_Already_Join_Team_RED = "�7Tu est d�j� dans l'�quipe " + ChatColor.RED + "Rouge";
	static String Game_Already_Join_Team_BLUE = "�7Tu est d�j� dans l'�quipe " + ChatColor.BLUE + "Bleu";
	static String Game_Already_Join_Team_YELLOW = "�7Tu est d�j� dans l'�quipe " + ChatColor.YELLOW + "Jaune";
	static String Game_Already_Join_Team_GREEN = "�7Tu est d�j� dans l'�quipe " + ChatColor.GREEN + "Vert";
	static String Game_Start = "�eC'est parti, bonne chance !";
	static String Game_Enderchest_disable = "�cLes EnderChests sont d�sactiv� !";
	
	public static String getPrefix()
	{
		return prefix;
	}
	
	public static String getGovernorPrefix()
	{
		return GovernorPrefix;
	}
	
	public static String getMessageJoin()
	{
		return Join;
	}
	
	public static String getMessageAchatMissing()
	{
		return prefix + Achat_Missing;
	}
	
	public static String getMessageAchatCancelled()
	{
		return prefix + Achat_Cancelled;
	}
	
	public static String getMessageAchatSuccessful()
	{
		return prefix + Achat_Successful;
	}
	
	public static String getMessageAchatRefused()
	{
		return prefix + Achat_Refusedful;
	}
	
	public static String getMessageAchatRank()
	{
		return prefix + Achat_Rank;
	}
	
	public static String getMessageAchatAlready()
	{
		return prefix + Achat_Already;
	}
	
	public static String getMessageNoPermission()
	{
		return prefix + no_permission;
	}
	
	public static String getMessageGameFinishJoinLobby()
	{
		return prefix + Game_Finish_Join_Lobby;
	}
	
	public static String getMessageGameOnlySpectatorFull()
	{
		return prefix + Game_Only_Spectator_Full;
	}
	
	public static String getMessageGameJoinTeamRed()
	{
		return prefix + Game_Join_Team_RED;
	}
	
	public static String getMessageGameJoinTeamBlue()
	{
		return prefix + Game_Join_Team_BLUE;
	}
	
	public static String getMessageGameJoinTeamYellow()
	{
		return prefix + Game_Join_Team_YELLOW;
	}
	
	public static String getMessageGameJoinTeamGreen()
	{
		return prefix + Game_Join_Team_GREEN;
	}
	
	public static String getMessageGameStart()
	{
		return prefix + Game_Start;
	}
	
	public static String getMessageGameEnderchestDisable()
	{
		return prefix + Game_Enderchest_disable;
	}
	
	public static String getMessageGameJoinTeamRedFull()
	{
		return prefix + Game_Join_Team_RED_Full;
	}
	
	public static String getMessageGameJoinTeamBlueFull()
	{
		return prefix + Game_Join_Team_BLUE_Full;
	}
	
	public static String getMessageGameJoinTeamYellowFull()
	{
		return prefix + Game_Join_Team_YELLOW_Full;
	}
	
	public static String getMessageGameJoinTeamGreenFull()
	{
		return prefix + Game_Join_Team_GREEN_Full;
	}
	
	public static String getMessageGameAlreadyJoinTeamRed()
	{
		return prefix + Game_Already_Join_Team_RED;
	}
	
	public static String getMessageGameAlreadyJoinTeamBlue()
	{
		return prefix + Game_Already_Join_Team_BLUE;
	}
	
	public static String getMessageGameAlreadyJoinTeamYellow()
	{
		return prefix + Game_Already_Join_Team_YELLOW;
	}
	
	public static String getMessageGameAlreadyJoinTeamGreen()
	{
		return prefix + Game_Already_Join_Team_GREEN;
	}
}
