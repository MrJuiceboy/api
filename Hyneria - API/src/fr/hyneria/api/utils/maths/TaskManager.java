package fr.hyneria.api.utils.maths;

import fr.hyneria.api.HyneriaAPI;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class TaskManager {

    public static HashMap<String, Double> tasksTime 	= new HashMap<>();
    public static HashMap<String, Integer>	taskList	= new HashMap<>();
    public static BukkitScheduler scheduler	= Bukkit.getScheduler();
    static Plugin plugin		= HyneriaAPI.getInstance();
    private static String 					currentThreadName = Thread.currentThread().getName();

    /**
     * Charger le runnable
     * @param name
     * @param runnable
     * @return
     */
    private static Runnable loadRunnable(final String name, final Runnable runnable) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
		/*System.out.println("_____________ " + name + " ____________");
		for (StackTraceElement stackTraceElement : stackTraceElements) {
			System.out.println(stackTraceElement.toString());
		}*/
        return new Runnable() {
            @Override
            public void run() {
                long timeExecute = System.nanoTime();
                runnable.run();
                boolean sync = currentThreadName.equals(Thread.currentThread().getName());
                if (sync) {
                    double time = (System.nanoTime() - timeExecute) / 1_000_000D;
                    tasksTime.put(name, time);
                }
            }
        };
    }

    /**
     * Recup�re la task
     * @param taskName
     * @return
     */
    public BukkitTask getTask(String taskName) {
        BukkitTask task = null;
        int id = getTaskId(taskName);
        if (id > 0) {
            for (BukkitTask pendingTask : scheduler.getPendingTasks()) {
                if (pendingTask.getTaskId() == id) return task;
            }
        }
        return null;
    }

    /**
     * R�cup�rer l'utilisation de la t�che en millisecondes
     * @param task
     * @return
     */
    public static double getUsageTask(BukkitTask task) {
        String taskName = getTaskNameById(task.getTaskId());
        return !tasksTime.containsKey(taskName) ? -1D : tasksTime.get(taskName);
    }

    /**
     * R�cup�rer l'utilisation de toutes les t�ches en millisecondes
     * @return
     */
    public static Map<String, Double> getUsageTasks() {
        return tasksTime;
    }

    /**
     * Tente de r�cup�rer le nom de la task par l'id si elle existe encore et qu'elle a �t� d�clar� dans ce manager
     * @param id de la task
     * @return null si non trouv�
     */
    public static String getTaskNameById(int id) {
        for (Map.Entry<String, Integer> entry : taskList.entrySet()) {
            if (entry.getValue() == id) return entry.getKey();
        }
        return null;
    }

    // La t�che existe ?
    public static boolean taskExist(String taskName) {
        if (taskList.containsKey(taskName)) {
            return true;
        }
        return false;
    }

    // R�cup�ration de l'id
    public static int getTaskId(String taskName) {
        if (taskExist(taskName)) {
            return taskList.get(taskName);
        }
        return 0;
    }

    // Cancel all task
    public static void cancelAllTask() {
        for (int taskId : taskList.values()) {
            scheduler.cancelTask(taskId);
        }
    }

    // Cancel de la task by name
    public static boolean cancelTaskByName(String taskName) {
        if (taskExist(taskName)) {
            int taskId = getTaskId(taskName);
            taskList.remove(taskName);
            scheduler.cancelTask(taskId);
            return true;
        }
        return false;
    }

    // Annule une t�che par l'ID
    public static void cancelTaskById(int id) {
        scheduler.cancelTask(id);
    }

    public static void removeTaskByName(String taskName) {
        taskList.remove(taskName);
    }

    /**
     * Useless autant appel� directement TaskManager.cancelTaskByName(taskName) qui en plus retourne le status de
     * l'annulation
     * @deprecated
     */
    public static void checkIfExist(String taskName) {
        if (TaskManager.taskExist(taskName)) TaskManager.cancelTaskByName(taskName);
    }

    // Run task now
    public static BukkitTask runTask(Runnable runnable) {
        return scheduler.runTask(plugin, loadRunnable("runTask_" + (new Random().nextInt(6555555)), runnable));
    }

    /**
     * Run async task
     * @param runnable
     * @return
     */
    public static BukkitTask runTaskAsync(Runnable runnable) {
        return scheduler.runTaskAsynchronously(plugin, loadRunnable("runTask_" + (new Random().nextInt(6555555)), runnable));
    }

    // Run async task later
    public static BukkitTask runAsyncTaskLater(Runnable runnable, int tick) {
        return scheduler.runTaskLaterAsynchronously(plugin, loadRunnable("laterTask_" + tick + "_" + (new Random().nextInt(655555)), runnable), tick);
    }

    /**
     * Run task later
     * @param runnable
     * @param tick
     * @return
     */
    public static BukkitTask runTaskLater(Runnable runnable, int tick) {
        return scheduler.runTaskLater(plugin, loadRunnable("laterTask_" + tick + "_" + (new Random().nextInt(655555)), runnable), tick);
    }

    /**
     * Cr�er et enregistre une task, se retire de la liste toute seule � l'expiration, permet de l'annuler dans un
     * plugin et �viter les m�mory leaks
     * @param taskName
     * @param task
     * @param duration
     */
    public static BukkitTask runTaskLater(final String taskName, Runnable task, int duration) {
        BukkitTask bukkitTask = scheduler.runTaskLater(plugin, loadRunnable(taskName, task), duration);
        final int id = bukkitTask.getTaskId();
        TaskManager.addTask(taskName, id);
        runTaskLater(new Runnable() {
            @Override
            public void run() {
                // Toujours la m�me task ID pour �viter la suppression de task renouvel�es
                if (taskList.get(taskName) != null && taskList.get(taskName) == id) taskList.remove(taskName);
            }
        }, duration);
        return bukkitTask;
    }

    /**
     * Cr�er et enregistre une task, se retire de la liste toute seule � l'expiration, permet de l'annuler dans un
     * plugin et �viter les m�mory leaks
     * Tourne en async
     * @param taskName
     * @param task
     * @param duration
     */
    public static BukkitTask runAsyncTaskLater(final String taskName, Runnable task, int duration) {
        BukkitTask bukkitTask = scheduler.runTaskLaterAsynchronously(plugin, loadRunnable(taskName, task), duration);
        final int id = bukkitTask.getTaskId();
        TaskManager.addTask(taskName, id);
        runAsyncTaskLater(new Runnable() {
            @Override
            public void run() {
                // Toujours la m�me task ID pour �viter la suppression de task renouvel�es
                if (taskList.get(taskName) != null && taskList.get(taskName) == id) taskList.remove(taskName);
            }
        }, duration);
        return bukkitTask;
    }

    /**
     * Rajout une task dans la list
     * @param name
     * @param id
     */
    public static void addTask(String name, int id) {
        taskList.put(name, id);
    }

    /**
     * Ajoute une t�che r�p�titive Annule la pr�c�dante du meme nom si existe.
     * @param runnable
     * @param delay
     * @param refresh
     * @return
     */
    public static BukkitTask scheduleSyncRepeatingTask(String taskName, Runnable runnable, int delay, int refresh) {
        cancelTaskByName(taskName);
        BukkitTask task = scheduler.runTaskTimer(plugin, loadRunnable(taskName, runnable), delay, refresh);
        taskList.put(taskName, task.getTaskId());
        return task;
    }

    /**
     * Ajoute une t�che r�p�titive en async Annule la pr�c�dante du meme nom si existe.
     * @param runnable
     * @param delay
     * @param refresh
     * @return
     */
    public static BukkitTask scheduleAsyncRepeatingTask(String taskName, Runnable runnable, int delay, int refresh) {
        cancelTaskByName(taskName);
        BukkitTask task = scheduler.runTaskTimerAsynchronously(plugin, loadRunnable(taskName, runnable), delay, refresh);
        taskList.put(taskName, task.getTaskId());
        return task;
    }

    /**
     * Cr�er un nom de t�che unique bas� sur un nom de t�che
     * @param string
     * @return
     */
    public static String getTaskName(String string) {
        String taskName = string + "_" + new Random().nextInt(99999);
        while (taskExist(taskName)) {
            taskName = string + "_" + new Random().nextInt(99999);
        }
        return taskName;
    }

}

